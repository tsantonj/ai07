Analyse DB
===

# Lawnmower & Son

Vous travaillez en tant qu'ingénieur spécialisé dans les systèmes décisionnels au siège de l'entreprise internationale "Lawnmower & Son".
L'entreprise "Lawnmower & Son" produit de nombreuses références de tondeuses différentes sous sa marque. Chaque tondeuse est produite en France dans une des usines du groupe.
Le service qualité de chaque usine enregistre les défauts sur la chaà®ne de montage, au fur et à  mesure où ils sont relevés. Un ticket de défaut est généré à  chaque fois qu'une tondeuse présente un ou plusieurs défauts sur la chaà®ne. Tous les défauts relevés sont consignés sur le même ticket. Chaque défaut est identifié par un code de type de défaut, qui figurent dans le manuel qualité.
Le directeur qualité de l'entreprise souhaite savoir si, en analysant les données transactionnelles disponibles dans l'entreprise, il est possible de faire remonter des informations afin de l'aider à  faire des propositions permettant de réduire le nombre de défauts relevé sur les chaà®nes de montage dans les usines. Le spectre de la recherche doit être le plus large possible, et l'objectif est d'isoler des paramètres statistiquement significatifs concernant les relevés de défauts. Ces paramètres peuvent être aussi bien liés aux dates auxquelles sont relevés les défauts, qu'aux  caractéristiques des tondeuses ou aux usines où se produisent les défauts. Un axe important de décision consisterait à  pouvoir déterminer s'il vaut mieux travailler en priorité au niveau de l'organisation des usines (méthodes) ou de la conception des produits (études). On cherchera à  faciliter les questions concernant les tondeuses et les usines les plus souvent défectueuses. Il serait aussi opportun de caractériser les cas de défauts multiples relevés sur un même ticket, par exemple pour savoir s'ils sont plutôt de même catégorie ou non.



# Analyse des données
## Fichier `dpt_fr`

Nom: departement_insee_2003

Origine: [data.gouv](https://www.data.gouv.fr/fr/)

Format: .cvs
Header: non
Séparateur: `;`

Lignes: 95

Un axe important de décision consisterait à  pouvoir déterminer s'il vaut mieux travailler en priorité au niveau de l'organisation des usines (méthodes) ou de la conception des produits (études). On cherchera à  faciliter les questions concernant les tondeuses et les usines les plus souvent défectueuses. Il serait aussi opportun de caractériser les cas de défauts multiples relevés sur un même ticket, par exemple pour savoir s'ils sont plutôt de même catégorie ou non.

| Nom | Type | Description | Qualité | Utilité | Commentaire |
| :-----------: | :-------------: | :------------: | :-----------: | :-------------: | :------------: |
| Numéro | Varchar(2) | numéro du département | 3 | 3 |  |
| Nom | Varchar |  | 2 | 2 | certains caractères spéciaux ont des soucis d'affichage |
| Population | Varchar |  | 3 | 3 | quantité numérique |

Les données sembles totalement propres

## Fichier `Product`

Nom: Product

Origine: http://pic.crzt.fr/26/00006/Product

Format: .cvs
Header: non
Séparateur: `;`

lignes: 89

| Nom | Type | Description | Qualité | Utilité | Commentaire |
| :-----------: | :-------------: | :------------: | :-----------: | :-------------: | :------------: |
| Product | varchar(5) | Référence du produit dans le catalogue | 3 | 3 | :------------: |
| Color| char(7) | Code couleur du produit | 3 | 2 | donnée en hexa, RGB |
| Price | integer | Prix du produit | 3 | 3 |  |
| Size | integer | Taille du produit | 3 | 2 |  |
| Weight | integer | Poids du produit | 3 | 2 |  |
| AgeOfDesign | integer | age | 3 | 2 |  |

Les données sembles totalement propres


## Fichier `Lawnmower00003`

relevé des défauts 2017 - 2018

Non: lawnmower

Origine: http://pic.crzt.fr/26/00006/Lawnmower00003

Format: .cvs
Header: non
Séparateur: `;`

lignes: 4730

| Nom | Type | Description | Qualité | Utilité | Commentaire |
| :-----------: | :-------------: | :------------: | :-----------: | :-------------: | :------------: |
| Ticket number | varchar(11) | nombre entier | 3 | 3 |  |
| Date | Date | date | 3 | 3 | :------------: |
| Product | varchar(5) | ref produit | 3 | 3 |  |
| Factory | integer | ref usine | 3 | 3 |  |
| DefaultType | integer | type défaut | 3 | 3 |  |

Les données sembles totalement propres


## Fichier `Factory`

Nom: Factory

Origine: http://pic.crzt.fr/26/00006/Factory

Format: .cvs
Header: non
Séparateur: `;`

lignes: 42

| Nom | Type | Description | Qualité | Utilité | Commentaire |
| :-----------: | :-------------: | :------------: | :-----------: | :-------------: | :------------: |
| Factory | integer | Numéro de l'usine | 3 | 3 |  |
| AgeOfFactory | integer |  | 3 | 2 |  |
| Capacity | integer | Capacité de production de l'usine en nombre de produits par unité de temps. | 3 | 2 |  |
| FactoryLocation | char(2) | département où est située l'usine | 3 | 2 |  |

Les données sembles totalement propres


## Fichier `DefaultType`

nom: DefaultType

Origine: http://pic.crzt.fr/26/00006/DefaultType

Format: .cvs
Header: non
Séparateur: `;`

Lignes: 45

| Nom | Type | Description | Qualité | Utilité | Commentaire |
| :-----------: | :-------------: | :------------: | :-----------: | :-------------: | :------------: |
| DefaultType | integer | Numéro du type de défaut | 3 | 3 | :------------: |
| Category | char(1) | Categorie du défaut (code lettre) | 3 | 3 | parmi 'A', 'B', 'C', 'D', 'E', 'F' |

Les données sembles totalement propres


## Fichier `Category`

nom: Category

Origine: http://pic.crzt.fr/26/00006/Category

Format: .cvs
Header: non
Séparateur: `;`

Lignes: 6

| Nom | Type | Description | Qualité | Utilité | Commentaire |
| :-----------: | :-------------: | :------------: | :-----------: | :-------------: | :------------: |
| Category | Char(1) | Categorie du défaut (code lettre) | 3 | 3 | parmi 'A', 'B', 'C', 'D', 'E', 'F' |
| CategoryName | varchar(11) | Categorie du défaut (intitulé)| 3 | 3 | parmi 'Painting', 'Engine', 'Body', 'Electricity', 'Electronic', 'Equipment' |

Les données sembles totalement propres


#  Exemple de requête
Nous pouvons ainsi commencer par étudier si une catégorie reviens de manière plus récurrente qu'une autre:
_Quelle a été la quantité de défauts en fonction du type de défaut_
```
Quantité de défauts
/ catégorie
```

Notre requête SQL serait alors :
```sql
SELECT d.label_cat, count(*) AS total
	FROM fbdl_exam.tickets t, fbdl_exam.defauts d
	WHERE t.fk_defaut = d.num_defaut
	GROUP BY d.label_cat
	ORDER BY d.label_cat ASC;
```
# Modèle

![](model.png)
