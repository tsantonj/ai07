ai07_exam
===

This project contains every files to respond to a datawarehouse exam problem:
```
You work as a Business Intelligence Engineer at the headquarters of the international company "Lawnmower & Son".
"Lawnmower & Son" produces many different lawnmower references under its brand name. Each mower is produced in France in one of the group's factories.
The quality department of each plant records defects on the assembly line as they are detected. A defect ticket is generated each time a mower has one or more defects on the chain. All defects found are recorded on the same ticket. Each defect is identified by a defect type code, which is included in the quality manual.
The company's quality manager wants to know if, by analyzing the transactional data available in the company, it is possible to provide feedback to help him make proposals to reduce the number of defects found on assembly lines in plants. The research spectrum should be as broad as possible, and the objective is to isolate statistically significant parameters for defect records. These parameters can be related to the dates on which the defects are detected, as well as to the characteristics of the mowers or the plants where the defects occur. An important decision-making axis would be to be able to determine whether it is better to work primarily at the level of plant organization (methods) or product design (studies). Questions about the most frequently defective lawnmowers and plants will be facilitated. It would also be appropriate to characterize cases of multiple defects found on the same ticket, for example to determine whether they are of the same category or not.
```


# Getting Started

You can find the subject and the analyse of it and its datas in the folder `1_mod`.
The folder `2_etl` contains 3 sql scripts to create the 3 different zone of the ETL. You can also find a Python program to automatically run theses scripts and make a report stock in `log` file. `3_analyse` contains some results of the analyse of the ETL. You can find in it 4 questions asked in the exam, the python script to run the SQL queries and some graphics that correspond to the previous questions (in `fig` folder). Take a look on `analyse.md` or `analyse.html` to see results analyze. Finally, the folder `datas` contains every datas that were used in the exam, in CSV format.


# Prerequisites

To use Python script, you need following libraries:
- sys
- psycopg2
- re
- matplotlib
- datetime
- csv
- numpy
- pandas
- plotly

Moreover, please run these scripts on a Python3 installation.


# Installing

Use pipy to install the missing librairies

```
pip install psycopg2
```

To verify if every thing is installed, import every librairies in a Python file and run it.
```
import psycopg2
```


# Test
## ETL script
To launch Python script for the ETL, go into this folder `./ai07/ai07_exam_correction/2_etl/py_script/` and launch `python3 script.py`

## Reporting script
To launch Python script for the Reporting, go into this folder `./ai07/ai07_exam_correction/3_analyse/` and launch `python3 script.py`

## Command
```
cd <path_to_ai07_folder>
cd ./ai07/ai07_exam_correction/2_etl/py_script/
python3 script.py

cd ./ai07/ai07_exam_correction/3_analyse/
python3 script.py
```




# Authors

* **Thibault Santonja (bdd1p002)** - *Initial work* - [tsantonj](https://gitlab.utc.fr/tsantonj/ai07)
