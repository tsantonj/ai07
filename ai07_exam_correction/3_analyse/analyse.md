Analyse ETL
===

# Analyser les faits en fonction des catégories de défaut
```sql
CREATE VIEW nbdefautscategorie AS
  SELECT d.label_cat, count(*) AS total
    FROM fbdl_exam.tickets t, fbdl_exam.defauts d
    WHERE t.fk_defaut = d.num_defaut
    GROUP BY d.label_cat
    ORDER BY d.label_cat ASC;
SELECT * FROM nbdefautscategorie;
```

![nbdefautscategorie](./fig/default_category.png)

| label_cat  | total |
|:------------:|:------:|
| Body        |   205 |
| Electricity |   223 |
| Electronic  |   342 |
| Engine      |   289 |
| Equipment   |   385 |
| Painting    |  3286 |
(6 lignes)

On peut vite voir qu'une majorité des défauts sont des défauts de peinture.


![nbdefautstype](./fig/default_type.png)

On peut voir que les défauts de type 4,7,9,27,29,31,34,35,39 et 42 sont plus représentés que les autres.
Tous ces types de défauts correspondent à la catégorie "Painting". Comme nous l'avons vu précédemment, cette catégorie est en effet surreprésentée avec un total de 3286 défauts.


# Étudier les faits en fonction du jour de l'année.
```sql
CREATE VIEW defautjour AS
  SELECT d.date_full AS dates, COUNT(*) AS Tickets
    FROM fbdl_exam.dates d, fbdl_exam.tickets t
    WHERE d.date_full = t.fk_date
    GROUP BY dates
    ORDER BY dates ASC;
```

![defautjour](./fig/default_date.png)

Ce graphique étant peu lisible, nous allons relever les dates qui sortent du lot. On peut voir qu'il y a généralement moins de 20 défauts par jour, nous allons donc récupérer les jours où il y a plus de 20 défauts enregistrés.


| dates    | tickets |
|:------------:|:------:|
|2017-02-01 |      54|
|2017-02-25 |      43|
|2017-03-03 |      77|
|2017-03-23 |     101|
|2017-03-31 |      80|
|2017-05-05 |      42|
|2017-04-24 |      85|
|2017-05-17 |      30|
|2017-06-21 |      78|
|2017-06-17 |      63|
|2017-08-18 |      62|
|2017-09-05 |     134|
|2018-02-01 |      40|
|2018-02-25 |      40|
|2018-03-03 |      98|
|2018-03-23 |     122|
|2018-03-31 |      77|
|2018-04-24 |      74|
|2018-05-05 |      41|
|2018-05-17 |      51|
|2018-06-17 |      52|
|2018-06-21 |      92|
|2018-08-18 |      62|
|2018-09-05 |      95|
|2018-11-04 |      26|
(25 lignes)

Il est intéressant de voir que chaque année, c'est aux mêmes dates que nous pouvons relever des pics de défauts(3 mars 2017 avec 101 défauts et 3 mars 2018 avec 98 par exemple).


![default_jds](./fig/default_jds.png)

|jds    | tickets|
|:------------:|:------:|
|Dimanche |    555|
|Samedi   |    764|
|Vendredi |    859|
|Jeudi    |    675|
|Mercredi |    690|
|Mardi    |    660|
|Lundi    |    527|
(7 lignes)

On peut voir qu'il y a plus de défaut analysés les vendredis et les samedis. la différence n'est cependant pas

![default_month](./fig/default_month.png)

| month | tickets |
|:------------:|:------:|
|     1 |    289 |
|     2 |    386 |
|     3 |    779 |
|     4 |    425 |
|     5 |    386 |
|     6 |    539 |
|     7 |    263 |
|     8 |    400 |
|     9 |    436 |
|    10 |    240 |
|    11 |    324 |
|    12 |    263 |
(12 lignes)

On peut voir qu'il y a plus de défaut analysés en mars.


# Analyser les faits par rapport aux ages des produits
## Analyse par rapport à l'age
```sql
CREATE VIEW nbdefautsage AS
  SELECT p.design_age AS age, count(*) AS total
    FROM fbdl_exam.tickets t, fbdl_exam.produits p
    WHERE t.fk_produit = p.num_produit
    GROUP BY p.design_age
    ORDER BY p.design_age ASC;
SELECT * FROM nbdefautsage;
```

## Analyse par rapport à la tranche d'age
Rappel des categories:
- 1 : [0;4]
- 2 : [5;9]
- 3 : [10;14]
- 4 : [15;19]
- 5 : [20;25]
- 6 : [25;29]
- 7 : [30;34]
- 8 : [35; 39]
- 9 : [40+]

```sql
CREATE VIEW nbdefautstrancheage AS
  SELECT p.cat_age AS tranche_d_age, count(*) AS total
    FROM fbdl_exam.tickets t, fbdl_exam.produits p
    WHERE t.fk_produit = p.num_produit
    GROUP BY tranche_d_age
    ORDER BY tranche_d_age ASC;
SELECT * FROM nbdefautstrancheage;
```

![default_t_age](./fig/default_t_age.png)

| tranche_d_age | total |
|:------------:|:------:|
|            1 |   603 |
|            2 |   222 |
|            3 |   811 |
|            4 |   596 |
|            5 |   608 |
|            6 |   724 |
|            7 |   681 |
|            8 |   319 |
|            9 |   166 |
(9 lignes)

On peut voir qui on entre 10 et 14 ans sont ceux ayant le plus de défauts.
Il serait intéressant d'analyser le nombre de produit existant par tranche d'age.
```sql
CREATE VIEW tot_produit_p_age AS
  SELECT count(*) AS totalProduit, p.cat_age as P_t_age
    FROM fbdl_exam.produits p
    GROUP BY p.cat_age
    ORDER BY p.cat_age ASC;

CREATE VIEW rapport_defaut_produit AS
  SELECT d.tranche_d_age as tranche_age, d.total as total_defauts,
    t.totalProduit as total_produit, d.total/t.totalProduit as rapport_defaut_produit
    FROM nbdefautstrancheage d, tot_produit_p_age t
    WHERE d.tranche_d_age = t.P_t_age
    ORDER BY d.tranche_d_age ASC;
SELECT * FROM rapport_defaut_produit;
```
| tranche_age | total_defauts | total_produit | rapport_defaut_produit |
|:-----------:|:-------------:|:-------------:|:----------------------:|
|           1 |           603 |             2 |                    301 |
|           2 |           222 |             1 |                    222 |
|           3 |           811 |            10 |                     81 |
|           4 |           596 |            10 |                     59 |
|           5 |           608 |            11 |                     55 |
|           6 |           724 |            18 |                     40 |
|           7 |           681 |            19 |                     35 |
|           8 |           319 |            11 |                     29 |
|           9 |           166 |             7 |                     23 |
(9 lignes)

On observe que les produits récents ont plus de défauts que les anciens.
Nous avons pour les produits de moins de 5 ans, environ 301 défauts par produit.
Cependant cette analyse n'est pas encore juste. il faudrait comparer ce résultat avec le nombre de produit en circulation. on peut supposer que les produits récents sont plus présents que les anciens, expliquant en partie de ce fait la sur présence de défauts sur les produits de moins de 10 ans, comparé à ceux de plus de 10 ans.


# Analyser les faits en fonction de la capacité des usines
```sql
CREATE VIEW defautusinecapac AS
   SELECT u.capacite, count(*) AS totalDefauts
     FROM fbdl_exam.usines u, fbdl_exam.tickets t
     WHERE t.fk_usine = u.num_usine
     GROUP BY u.capacite
     ORDER BY u.capacite ASC;

SELECT * FROM defautusinecapac;
```
![default_capacity](./fig/default_capacity.png)

|capacite | totaldefauts|
|:------------:|:------:|
|        2 |           37|
|        3 |          178|
|        4 |          359|
|        5 |          522|
|        6 |          833|
|        7 |          995|
|        8 |         1260|
|        9 |          546|
(8 lignes)

```sql
CREATE VIEW tot_usine_p_capacite AS
   SELECT count(*) AS totalUsines, u.capacite
     FROM fbdl_exam.usines u
     GROUP BY u.capacite
     ORDER BY u.capacite ASC;

CREATE  VIEW rapport_defaut_usine AS
   SELECT d.capacite, d.totalDefauts, t.totalUsines,
     d.totalDefauts/t.totalUsines as rapport_defaut_usine
     FROM defautusinecapac d, tot_usine_p_capacite t
     WHERE d.capacite = t.capacite
     ORDER BY d.capacite ASC;

SELECT * FROM rapport_defaut_usine;
```
| capacite | totaldefauts | totalusines | rapport_defaut_usine |
|:------------:|:------:|:------------:|:------:|
|        2 |           37 |           3 |                   12|
|        3 |          178 |           6 |                   29|
|        4 |          359 |           6 |                   59|
|        5 |          522 |           6 |                   87|
|        6 |          833 |           7 |                  119|
|        7 |          995 |           6 |                  165|
|        8 |         1260 |           6 |                  210|
|        9 |          546 |           2 |                  273|
(8 lignes)
