# -*- coding: utf-8 -*-

# File Name     : script
# Description   : python script to query the data warehouse and analyze the results
#
# Requirements  : matplotlib, psycopg2
#
# Version       : 1.0
# Created on    : Wed Jun  5 12:51:11 2019
#
# @author       : Thibault Santonja
# git depot     :
#
# =============================================================================

import matplotlib.pyplot as plt
import psycopg2

import queries


HOST = 'tuxa.sme.utc'
DB = 'dbbdd1p002'
USER = 'bdd1p002'
PASSWD = input('enter the password: ')

# connexion function to pgsql database
def db_connexion():
    try:
        conn = psycopg2.connect(
                host =        HOST,
                dbname =    DB,
                user =         USER,
                password =     PASSWD
            )
        cursor = conn.cursor()

    except Exception as e:
        raise e

    return conn, cursor


# create every views to analyse the data warehouse
def create_views(conn, cursor):
    for query in queries.queries:
        cursor.execute(query)
        conn.commit()


# function to request the data warehouse and stock / print results
# WARNING: the only 2 first columns will be printed in the plot !
#
# request(conn, cursor, query, [x_label=], [y_label=], [title=], [log_file=]):
def request(conn, cursor, query, **kwargs):
    # get the **kwargs parameters
    x_label = kwargs.get('x_label', 'X')
    y_label = kwargs.get('y_label', 'Y')
    title = kwargs.get('title', '')
    log_file = kwargs.get('log_file', None)

    # execute the query
    cursor.execute(query)

    # if a reporting file have been passed in parameters, prepare it
    if log_file:
        log_file.write('\n\n# analyze: ' + title)
        log_file.write('\n\n| ' + x_label + ' | ' + y_label + ' |')
        log_file.write('\n| --- | --- |')

    x = []
    y = []
    # get the results and write it in the reporting file if once have been
    # passed in parameters
    for row in cursor:
        if log_file:
            log_file.write('\n| ' + str(row[0]) + ' | ' + str(row[1]) + ' |')

        x.append(row[0])
        y.append(row[1])

    # bar plot configuration and printing
    plt.bar(range(len(x)), y, align='center')
    plt.xticks(range(len(x)), x)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.show()


if __name__ == "__main__":
    # reporting.md cleanning
    open('reporting.md', 'w').close()

    # re openning reporting.md with 'a' option
    log_file = open('reporting.md', 'a')
    log_file.write('Reporting ETL\n===\n')
    conn, cursor = db_connexion()

    # views creation
    create_views(conn, cursor)

    # execute each queries to request the data warehouse
    query = """SELECT * FROM nbdefautstype;"""
    x_label = 'default type'
    y_label = 'total default'
    title = 'total default by type'
    request(conn, cursor, query, x_label=x_label, y_label=y_label, title=title, log_file=log_file)

    query = """SELECT * FROM nbdefautscategorie;"""
    x_label = 'category'
    y_label = 'total default'
    title = 'total default by category'
    request(conn, cursor, query, x_label=x_label, y_label=y_label, title=title, log_file=log_file)

    # WARNING: for this one, due to a large amount of datas, the results will
    # not be wrote in the log file.
    query = """SELECT * FROM defautjour;"""
    x_label = 'date'
    y_label = 'total default'
    title = 'total default by date'
    request(conn, cursor, query, x_label=x_label, y_label=y_label, title=title)

    query = """SELECT * FROM defautjourbis;"""
    x_label = 'date'
    y_label = 'total default'
    title = 'total default by date where total is more than 20'
    request(conn, cursor, query, x_label=x_label, y_label=y_label, title=title, log_file=log_file)

    query = """SELECT * FROM defautjds;"""
    x_label = 'jds'
    y_label = 'total default'
    title = 'total default by jds'
    request(conn, cursor, query, x_label=x_label, y_label=y_label, title=title, log_file=log_file)

    query = """SELECT * FROM defautmonth;"""
    x_label = 'month'
    y_label = 'total default'
    title = 'total default by month'
    request(conn, cursor, query, x_label=x_label, y_label=y_label, title=title, log_file=log_file)

    query = """SELECT * FROM defauttrim;"""
    x_label = 'trimester'
    y_label = 'total default'
    title = 'total default by trimester'
    request(conn, cursor, query, x_label=x_label, y_label=y_label, title=title, log_file=log_file)

    query = """SELECT * FROM nbdefautstrancheage;"""
    x_label = 'age'
    y_label = 'total default'
    title = 'total default by age'
    request(conn, cursor, query, x_label=x_label, y_label=y_label, title=title, log_file=log_file)

    query = """SELECT * FROM rapport_defaut_produit;"""
    x_label = 'product'
    y_label = 'total default'
    title = 'total default by product'
    request(conn, cursor, query, x_label=x_label, y_label=y_label, title=title, log_file=log_file)

    query = """SELECT * FROM rapport_defaut_usine;"""
    x_label = 'product'
    y_label = 'total default'
    title = 'total default by usine'
    request(conn, cursor, query, x_label=x_label, y_label=y_label, title=title, log_file=log_file)

    cursor.close()
    conn.close()
    log_file.close()

    print("\nReporting done.\n0")
