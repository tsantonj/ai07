# -*- coding: utf-8 -*-

# File Name     : queries
# Description   : this file contains all the SQL queries to execute
#
# Requirements  : sys
#
# Version       : 1.0
# Created on    : Wed Jun  5 13:03:03 2019
#
# @author       : Thibault Santonja
# git depot     :
#
# =============================================================================


# SELECT * FROM nbdefautscategorie;
nbdefautscategorie = """
DROP VIEW IF EXISTS nbdefautscategorie CASCADE;
CREATE VIEW nbdefautscategorie AS
  SELECT d.label_cat, count(*) AS total
    FROM fbdl_exam.tickets t, fbdl_exam.defauts d
    WHERE t.fk_defaut = d.num_defaut
    GROUP BY d.label_cat
    ORDER BY d.label_cat ASC;
"""

# SELECT * FROM nbdefautscategorie;
nbdefautstype = """
DROP VIEW IF EXISTS nbdefautstype CASCADE;
CREATE VIEW nbdefautstype AS
  SELECT d.num_defaut, count(*) AS total
    FROM fbdl_exam.tickets t, fbdl_exam.defauts d
    WHERE t.fk_defaut = d.num_defaut
    GROUP BY d.num_defaut
    ORDER BY d.num_defaut ASC;
"""

# SELECT * FROM defautjour;
defautjour = """
DROP VIEW IF EXISTS defautjour CASCADE;
CREATE VIEW defautjour AS
  SELECT d.date_full AS dates, COUNT(*) AS Tickets
    FROM fbdl_exam.dates d, fbdl_exam.tickets t
    WHERE d.date_full = t.fk_date
    GROUP BY dates
    ORDER BY dates ASC;
"""

# SELECT * FROM defautjour;
defautjourbis = """
DROP VIEW IF EXISTS defautjourbis CASCADE;
CREATE VIEW defautjourbis AS
  SELECT dates, tickets
    FROM defautjour
    WHERE tickets > 20
    ORDER BY dates ASC;
"""

# SELECT * FROM defautjds;
defautjds = """
DROP VIEW IF EXISTS defautjds CASCADE;
CREATE VIEW defautjds AS
  SELECT d.jour_semaine AS JDS, COUNT(*) AS Tickets
    FROM fbdl_exam.dates d, fbdl_exam.tickets t
    WHERE d.date_full = t.fk_date
    GROUP BY d.jour_semaine
    ORDER BY d.jour_semaine='Lundi', d.jour_semaine='Mardi',
      d.jour_semaine='Mercredi', d.jour_semaine='Jeudi', d.jour_semaine='Vendredi',
      d.jour_semaine='Samedi', d.jour_semaine='Dimanche';
"""

# SELECT * FROM defautmonth;
defautmonth = """
DROP VIEW IF EXISTS defautmonth CASCADE;
CREATE VIEW defautmonth AS
  SELECT d.mois AS month, COUNT(*) AS Tickets
    FROM fbdl_exam.dates d, fbdl_exam.tickets t
    WHERE d.date_full = t.fk_date
    GROUP BY month
    ORDER BY month ASC;
"""

# SELECT * FROM defauttrim;
defauttrim = """
DROP VIEW IF EXISTS defauttrim CASCADE;
CREATE VIEW defauttrim AS
  SELECT d.trimestre AS trimestre, COUNT(*) AS Tickets
    FROM fbdl_exam.dates d, fbdl_exam.tickets t
    WHERE d.date_full = t.fk_date
    GROUP BY trimestre
    ORDER BY trimestre ASC;
"""

# SELECT * FROM nbdefautsage;
nbdefautsage = """
DROP VIEW IF EXISTS nbdefautsage CASCADE;
CREATE VIEW nbdefautsage AS
  SELECT p.design_age AS age, count(*) AS total
    FROM fbdl_exam.tickets t, fbdl_exam.produits p
    WHERE t.fk_produit = p.num_produit
    GROUP BY p.design_age
    ORDER BY p.design_age ASC;
"""

# SELECT * FROM nbdefautstrancheage;
nbdefautstrancheage = """
DROP VIEW IF EXISTS nbdefautstrancheage CASCADE;
CREATE VIEW nbdefautstrancheage AS
  SELECT p.cat_age AS tranche_d_age, count(*) AS total
    FROM fbdl_exam.tickets t, fbdl_exam.produits p
    WHERE t.fk_produit = p.num_produit
    GROUP BY tranche_d_age
    ORDER BY tranche_d_age ASC;
"""

# SELECT * FROM rapport_defaut_produit;
rapport_defaut_produit = """
DROP VIEW IF EXISTS tot_produit_p_age CASCADE;
CREATE VIEW tot_produit_p_age AS
  SELECT count(*) AS totalProduit, p.cat_age as P_t_age
    FROM fbdl_exam.produits p
    GROUP BY p.cat_age
    ORDER BY p.cat_age ASC;

DROP VIEW IF EXISTS rapport_defaut_produit CASCADE;
CREATE VIEW rapport_defaut_produit AS
  SELECT d.tranche_d_age as tranche_age, d.total as total_defauts,
    t.totalProduit as total_produit, d.total/t.totalProduit as rapport_defaut_produit
    FROM nbdefautstrancheage d, tot_produit_p_age t
    WHERE d.tranche_d_age = t.P_t_age
    ORDER BY d.tranche_d_age ASC;
"""


defautusinecapac = """
DROP VIEW IF EXISTS defautusinecapac CASCADE;
CREATE VIEW defautusinecapac AS
   SELECT u.capacite, count(*) AS totalDefauts
     FROM fbdl_exam.usines u, fbdl_exam.tickets t
     WHERE t.fk_usine = u.num_usine
     GROUP BY u.capacite
     ORDER BY u.capacite ASC;
"""

rapport_defaut_usine = """
DROP VIEW IF EXISTS tot_usine_p_capacite CASCADE;
CREATE VIEW tot_usine_p_capacite AS
   SELECT count(*) AS totalUsines, u.capacite
     FROM fbdl_exam.usines u
     GROUP BY u.capacite
     ORDER BY u.capacite ASC;

DROP VIEW IF EXISTS rapport_defaut_usine CASCADE;
CREATE  VIEW rapport_defaut_usine AS
   SELECT d.capacite, d.totalDefauts, t.totalUsines,
     d.totalDefauts/t.totalUsines as rapport_defaut_usine
     FROM defautusinecapac d, tot_usine_p_capacite t
     WHERE d.capacite = t.capacite
     ORDER BY d.capacite ASC;
"""

queries = [nbdefautscategorie, defautjour, defautjourbis, defautjds, defautmonth, defauttrim, nbdefautsage, nbdefautstrancheage, rapport_defaut_produit, nbdefautstype, defautusinecapac, rapport_defaut_usine]
