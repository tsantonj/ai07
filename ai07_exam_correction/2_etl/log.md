log ETL
===


# E zone

-= E zone =-
drop & create table...
done !
insert datas...
done !
table list:[['produit'], ['departement'], ['defaut'], ['factory'], ['deftype'], ['cat']]

## Table produit
total lines: 89
10 first lines: 
|	p_num	|	color	|	price	|	size	|	weight	|	d_age	|
|---|---|---|---|---|---|
|	REF1	|	#bcac12	|	1099	|	187	|	1821	|	12	|
|	REF2	|	#26aff4	|	369	|	179	|	1793	|	36	|
|	REF3	|	#beaff2	|	894	|	234	|	2233	|	30	|
|	REF4	|	#b72a56	|	1381	|	265	|	2451	|	40	|
|	REF5	|	#bfdb42	|	1302	|	192	|	2253	|	34	|
|	REF6	|	#612fb6	|	1724	|	203	|	2396	|	34	|
|	REF7	|	#e92b32	|	1360	|	42	|	364	|	35	|
|	REF8	|	#d33bb4	|	1352	|	175	|	1404	|	36	|
|	REF9	|	#f128f4	|	151	|	113	|	1322	|	18	|
|	REF10	|	#033db6	|	971	|	131	|	1102	|	31	|

## Table departement
total lines: 95
10 first lines: 
|	num	|	nom	|	pop	|
|---|---|---|
|	1	|	Ain	|	529378	|
|	2	|	Aisne	|	552320	|
|	3	|	Allier	|	357110	|
|	4	|	Alpes-de-Haute-Provence	|	144809	|
|	5	|	Hautes-Alpes	|	126636	|
|	6	|	Alpes-Maritimes	|	1022710	|
|	7	|	Ardèche	|	294522	|
|	8	|	Ardennes	|	299166	|
|	9	|	Ariège	|	142834	|
|	10	|	Aube	|	301388	|

## Table defaut
total lines: 4730
10 first lines: 
|	num	|	dt	|	prod	|	fact	|	type	|
|---|---|---|---|---|
|	16290000001	|	2017-01-01	|	REF30	|	17	|	36	|
|	6680000002	|	2017-01-01	|	REF69	|	7	|	20	|
|	6680000002	|	2017-01-01	|	REF69	|	7	|	42	|
|	6680000003	|	2017-01-01	|	REF69	|	7	|	31	|
|	4030000003	|	2017-01-01	|	REF4	|	5	|	34	|
|	4030000004	|	2017-01-01	|	REF4	|	5	|	40	|
|	23810000005	|	2017-01-01	|	REF82	|	24	|	31	|
|	13860000006	|	2017-01-01	|	REF87	|	14	|	42	|
|	4720010006	|	2017-01-02	|	REF73	|	5	|	35	|
|	9830020007	|	2017-01-03	|	REF84	|	10	|	42	|

## Table factory
total lines: 42
10 first lines: 
|	num	|	age	|	capa	|	loc	|
|---|---|---|---|
|	1	|	8	|	2	|	44	|
|	2	|	9	|	3	|	49	|
|	3	|	47	|	9	|	27	|
|	4	|	19	|	6	|	91	|
|	5	|	7	|	3	|	37	|
|	6	|	28	|	8	|	39	|
|	7	|	13	|	8	|	90	|
|	8	|	12	|	4	|	26	|
|	9	|	9	|	7	|	94	|
|	10	|	8	|	4	|	69	|

## Table deftype
total lines: 45
10 first lines: 
|	type	|	cat	|
|---|---|
|	1	|	C	|
|	2	|	F	|
|	3	|	F	|
|	4	|	A	|
|	5	|	B	|
|	6	|	E	|
|	7	|	A	|
|	8	|	D	|
|	9	|	A	|
|	10	|	E	|

## Table cat
total lines: 6
10 first lines: 
|	cat	|	name	|
|---|---|
|	A	|	Painting	|
|	B	|	Engine	|
|	C	|	Body	|
|	D	|	Electricity	|
|	E	|	Electronic	|
|	F	|	Equipment	|


# T zone

-= T zone =-
drop & create table...
done !
insert datas...
done !
transform datas...
done !
table list:[['tmp_dt'], ['produit'], ['departement'], ['defaut'], ['factory'], ['deftype'], ['cat'], ['vdate'], ['vticket'], ['vfactory'], ['vproduit'], ['vdefaut']]

## Table tmp_dt
total lines: 668
10 first lines: 
|	dt	|
|---|
|	2017-10-17	|
|	2018-01-30	|
|	2018-08-14	|
|	2018-06-01	|
|	2017-05-26	|
|	2017-05-05	|
|	2018-01-25	|
|	2018-01-27	|
|	2018-10-26	|
|	2017-01-26	|

## Table produit
total lines: 89
10 first lines: 
|	p_num	|	color	|	price	|	size	|	weight	|	d_age	|
|---|---|---|---|---|---|
|	REF1	|	#bcac12	|	1099	|	187	|	1821	|	12	|
|	REF2	|	#26aff4	|	369	|	179	|	1793	|	36	|
|	REF3	|	#beaff2	|	894	|	234	|	2233	|	30	|
|	REF4	|	#b72a56	|	1381	|	265	|	2451	|	40	|
|	REF5	|	#bfdb42	|	1302	|	192	|	2253	|	34	|
|	REF6	|	#612fb6	|	1724	|	203	|	2396	|	34	|
|	REF7	|	#e92b32	|	1360	|	42	|	364	|	35	|
|	REF8	|	#d33bb4	|	1352	|	175	|	1404	|	36	|
|	REF9	|	#f128f4	|	151	|	113	|	1322	|	18	|
|	REF10	|	#033db6	|	971	|	131	|	1102	|	31	|

## Table departement
total lines: 95
10 first lines: 
|	num	|	nom	|	pop	|
|---|---|---|
|	1 	|	Ain	|	529378	|
|	2 	|	Aisne	|	552320	|
|	3 	|	Allier	|	357110	|
|	4 	|	Alpes-de-Haute-Provence	|	144809	|
|	5 	|	Hautes-Alpes	|	126636	|
|	6 	|	Alpes-Maritimes	|	1022710	|
|	7 	|	Ardèche	|	294522	|
|	8 	|	Ardennes	|	299166	|
|	9 	|	Ariège	|	142834	|
|	10	|	Aube	|	301388	|

## Table defaut
total lines: 4730
10 first lines: 
|	num	|	dt	|	prod	|	fact	|	type	|
|---|---|---|---|---|
|	16290000001	|	2017-01-01	|	REF30	|	17	|	36	|
|	6680000002	|	2017-01-01	|	REF69	|	7	|	20	|
|	6680000002	|	2017-01-01	|	REF69	|	7	|	42	|
|	6680000003	|	2017-01-01	|	REF69	|	7	|	31	|
|	4030000003	|	2017-01-01	|	REF4	|	5	|	34	|
|	4030000004	|	2017-01-01	|	REF4	|	5	|	40	|
|	23810000005	|	2017-01-01	|	REF82	|	24	|	31	|
|	13860000006	|	2017-01-01	|	REF87	|	14	|	42	|
|	4720010006	|	2017-01-02	|	REF73	|	5	|	35	|
|	9830020007	|	2017-01-03	|	REF84	|	10	|	42	|

## Table factory
total lines: 42
10 first lines: 
|	num	|	age	|	capa	|	loc	|
|---|---|---|---|
|	1	|	8	|	2	|	44	|
|	2	|	9	|	3	|	49	|
|	3	|	47	|	9	|	27	|
|	4	|	19	|	6	|	91	|
|	5	|	7	|	3	|	37	|
|	6	|	28	|	8	|	39	|
|	7	|	13	|	8	|	90	|
|	8	|	12	|	4	|	26	|
|	9	|	9	|	7	|	94	|
|	10	|	8	|	4	|	69	|

## Table deftype
total lines: 45
10 first lines: 
|	type	|	cat	|
|---|---|
|	1	|	C	|
|	2	|	F	|
|	3	|	F	|
|	4	|	A	|
|	5	|	B	|
|	6	|	E	|
|	7	|	A	|
|	8	|	D	|
|	9	|	A	|
|	10	|	E	|

## Table cat
total lines: 6
10 first lines: 
|	cat	|	name	|
|---|---|
|	A	|	Painting	|
|	B	|	Engine	|
|	C	|	Body	|
|	D	|	Electricity	|
|	E	|	Electronic	|
|	F	|	Equipment	|

## Table vdate
total lines: 668
10 first lines: 
|	dt	|	annee	|	trimestre	|	mois	|	semaine	|	joursemaine	|	jourannee	|
|---|---|---|---|---|---|---|
|	2017-10-17	|	2017.0	|	4.0	|	10.0	|	42.0	|	Mardi	|	290.0	|
|	2018-01-30	|	2018.0	|	1.0	|	1.0	|	5.0	|	Mardi	|	30.0	|
|	2018-08-14	|	2018.0	|	3.0	|	8.0	|	33.0	|	Mardi	|	226.0	|
|	2018-06-01	|	2018.0	|	2.0	|	6.0	|	22.0	|	Vendredi	|	152.0	|
|	2017-05-26	|	2017.0	|	2.0	|	5.0	|	21.0	|	Vendredi	|	146.0	|
|	2017-05-05	|	2017.0	|	2.0	|	5.0	|	18.0	|	Vendredi	|	125.0	|
|	2018-01-25	|	2018.0	|	1.0	|	1.0	|	4.0	|	Jeudi	|	25.0	|
|	2018-01-27	|	2018.0	|	1.0	|	1.0	|	4.0	|	Samedi	|	27.0	|
|	2018-10-26	|	2018.0	|	4.0	|	10.0	|	43.0	|	Vendredi	|	299.0	|
|	2017-01-26	|	2017.0	|	1.0	|	1.0	|	4.0	|	Jeudi	|	26.0	|

## Table vticket
total lines: 4730
10 first lines: 
|	num	|	dt	|	prod	|	fact	|	type	|
|---|---|---|---|---|
|	16290000001	|	2017-01-01	|	REF30	|	17	|	36	|
|	6680000002	|	2017-01-01	|	REF69	|	7	|	20	|
|	6680000002	|	2017-01-01	|	REF69	|	7	|	42	|
|	6680000003	|	2017-01-01	|	REF69	|	7	|	31	|
|	4030000003	|	2017-01-01	|	REF4	|	5	|	34	|
|	4030000004	|	2017-01-01	|	REF4	|	5	|	40	|
|	23810000005	|	2017-01-01	|	REF82	|	24	|	31	|
|	13860000006	|	2017-01-01	|	REF87	|	14	|	42	|
|	4720010006	|	2017-01-02	|	REF73	|	5	|	35	|
|	9830020007	|	2017-01-03	|	REF84	|	10	|	42	|

## Table vfactory
total lines: 42
10 first lines: 
|	num	|	age	|	s_age	|	capa	|	loc	|	nom	|	pop	|	top_usine	|
|---|---|---|---|---|---|---|---|
|	1	|	8	|	1	|	2	|	44	|	Loire-Atlantique	|	1165231	|	0	|
|	2	|	9	|	1	|	3	|	49	|	Maine-et-Loire	|	757130	|	0	|
|	3	|	47	|	5	|	9	|	27	|	Eure	|	557049	|	1	|
|	4	|	19	|	2	|	6	|	91	|	Essonne	|	1149368	|	0	|
|	5	|	7	|	1	|	3	|	37	|	Indre-et-Loire	|	568031	|	0	|
|	6	|	28	|	3	|	8	|	39	|	Jura	|	261922	|	1	|
|	7	|	13	|	2	|	8	|	90	|	Territoire de Belfort	|	142098	|	1	|
|	8	|	12	|	2	|	4	|	26	|	Drôme	|	450732	|	0	|
|	9	|	9	|	1	|	7	|	94	|	Val-de-Marne	|	1236786	|	0	|
|	10	|	8	|	1	|	4	|	69	|	Rhône	|	1605847	|	0	|

## Table vproduit
total lines: 89
10 first lines: 
|	num	|	color	|	price	|	size	|	weight	|	d_age	|	cat_age	|
|---|---|---|---|---|---|---|
|	REF1	|	#bcac12	|	1099	|	187	|	1821	|	12	|	3	|
|	REF2	|	#26aff4	|	369	|	179	|	1793	|	36	|	8	|
|	REF3	|	#beaff2	|	894	|	234	|	2233	|	30	|	7	|
|	REF4	|	#b72a56	|	1381	|	265	|	2451	|	40	|	9	|
|	REF5	|	#bfdb42	|	1302	|	192	|	2253	|	34	|	7	|
|	REF6	|	#612fb6	|	1724	|	203	|	2396	|	34	|	7	|
|	REF7	|	#e92b32	|	1360	|	42	|	364	|	35	|	8	|
|	REF8	|	#d33bb4	|	1352	|	175	|	1404	|	36	|	8	|
|	REF9	|	#f128f4	|	151	|	113	|	1322	|	18	|	4	|
|	REF10	|	#033db6	|	971	|	131	|	1102	|	31	|	7	|

## Table vdefaut
total lines: 45
10 first lines: 
|	type	|	cat	|	name	|	top_default	|
|---|---|---|---|
|	1	|	C	|	Body	|	0	|
|	2	|	F	|	Equipment	|	0	|
|	3	|	F	|	Equipment	|	0	|
|	4	|	A	|	Painting	|	1	|
|	5	|	B	|	Engine	|	0	|
|	6	|	E	|	Electronic	|	0	|
|	7	|	A	|	Painting	|	1	|
|	8	|	D	|	Electricity	|	0	|
|	9	|	A	|	Painting	|	1	|
|	10	|	E	|	Electronic	|	0	|


# L zone

-= L zone =-
drop & create table...
done !
insert datas...
done !
constraint table...
done !
table list:[['usines'], ['dates'], ['produits'], ['defauts'], ['tickets']]

## Table usines
total lines: 42
10 first lines: 
|	num_usine	|	age	|	capacite	|	localisation	|	nom_dep	|	pop_dep	|	cat_age	|	top_usine	|
|---|---|---|---|---|---|---|---|
|	15	|	14	|	6	|	11	|	Aude	|	319611	|	2	|	0	|
|	24	|	9	|	5	|	17	|	Charente-Maritime	|	579187	|	1	|	0	|
|	42	|	24	|	4	|	18	|	Cher	|	325131	|	3	|	0	|
|	38	|	40	|	4	|	18	|	Cher	|	325131	|	5	|	0	|
|	29	|	40	|	5	|	20	|	Corse	|	267249	|	5	|	0	|
|	8	|	12	|	4	|	26	|	Drôme	|	450732	|	2	|	0	|
|	3	|	47	|	9	|	27	|	Eure	|	557049	|	5	|	1	|
|	40	|	37	|	4	|	30	|	Gard	|	638198	|	4	|	0	|
|	32	|	23	|	9	|	33	|	Gironde	|	1315380	|	3	|	1	|
|	5	|	7	|	3	|	37	|	Indre-et-Loire	|	568031	|	1	|	0	|

## Table dates
total lines: 668
10 first lines: 
|	date_full	|	annee	|	trimestre	|	mois	|	semaine	|	jour_semaine	|	jour_annee	|
|---|---|---|---|---|---|---|
|	2017-10-17	|	2017	|	4	|	10	|	42	|	Mardi	|	290	|
|	2018-01-30	|	2018	|	1	|	1	|	5	|	Mardi	|	30	|
|	2018-08-14	|	2018	|	3	|	8	|	33	|	Mardi	|	226	|
|	2018-06-01	|	2018	|	2	|	6	|	22	|	Vendredi	|	152	|
|	2017-05-26	|	2017	|	2	|	5	|	21	|	Vendredi	|	146	|
|	2017-05-05	|	2017	|	2	|	5	|	18	|	Vendredi	|	125	|
|	2018-01-25	|	2018	|	1	|	1	|	4	|	Jeudi	|	25	|
|	2018-01-27	|	2018	|	1	|	1	|	4	|	Samedi	|	27	|
|	2018-10-26	|	2018	|	4	|	10	|	43	|	Vendredi	|	299	|
|	2017-01-26	|	2017	|	1	|	1	|	4	|	Jeudi	|	26	|

## Table produits
total lines: 89
10 first lines: 
|	num_produit	|	couleur	|	prix	|	taille	|	poids	|	design_age	|	cat_age	|
|---|---|---|---|---|---|---|
|	REF1	|	#bcac12	|	1099	|	187	|	1821	|	12	|	3	|
|	REF2	|	#26aff4	|	369	|	179	|	1793	|	36	|	8	|
|	REF3	|	#beaff2	|	894	|	234	|	2233	|	30	|	7	|
|	REF4	|	#b72a56	|	1381	|	265	|	2451	|	40	|	9	|
|	REF5	|	#bfdb42	|	1302	|	192	|	2253	|	34	|	7	|
|	REF6	|	#612fb6	|	1724	|	203	|	2396	|	34	|	7	|
|	REF7	|	#e92b32	|	1360	|	42	|	364	|	35	|	8	|
|	REF8	|	#d33bb4	|	1352	|	175	|	1404	|	36	|	8	|
|	REF9	|	#f128f4	|	151	|	113	|	1322	|	18	|	4	|
|	REF10	|	#033db6	|	971	|	131	|	1102	|	31	|	7	|

## Table defauts
total lines: 45
10 first lines: 
|	num_defaut	|	categorie	|	label_cat	|	top_defaut	|
|---|---|---|---|
|	29	|	A	|	Painting	|	1	|
|	27	|	A	|	Painting	|	0	|
|	4	|	A	|	Painting	|	1	|
|	7	|	A	|	Painting	|	1	|
|	9	|	A	|	Painting	|	1	|
|	42	|	A	|	Painting	|	0	|
|	39	|	A	|	Painting	|	0	|
|	35	|	A	|	Painting	|	1	|
|	34	|	A	|	Painting	|	0	|
|	31	|	A	|	Painting	|	0	|

## Table tickets
total lines: 4730
10 first lines: 
|	num_ticket	|	fk_usine	|	fk_date	|	fk_defaut	|	fk_produit	|	quantity	|
|---|---|---|---|---|---|
|	16290000001	|	17	|	2017-01-01	|	36	|	REF30	|	None	|
|	6680000002	|	7	|	2017-01-01	|	20	|	REF69	|	None	|
|	6680000002	|	7	|	2017-01-01	|	42	|	REF69	|	None	|
|	6680000003	|	7	|	2017-01-01	|	31	|	REF69	|	None	|
|	4030000003	|	5	|	2017-01-01	|	34	|	REF4	|	None	|
|	4030000004	|	5	|	2017-01-01	|	40	|	REF4	|	None	|
|	23810000005	|	24	|	2017-01-01	|	31	|	REF82	|	None	|
|	13860000006	|	14	|	2017-01-01	|	42	|	REF87	|	None	|
|	4720010006	|	5	|	2017-01-02	|	35	|	REF73	|	None	|
|	9830020007	|	10	|	2017-01-03	|	42	|	REF84	|	None	|


# DATA CONTROL

TABLE "produit" - FILE "../../datas/product.csv" OK.
Total lines in file: 89
Total lines in table: 89

TABLE "defaut" - FILE "../../datas/Lawnmower00003.csv" OK.
Total lines in file: 4730
Total lines in table: 4730

TABLE "departement" - FILE "../../datas/departement.csv" OK.
Total lines in file: 95
Total lines in table: 95

TABLE "cat" - FILE "../../datas/categorie.csv" OK.
Total lines in file: 6
Total lines in table: 6

TABLE "factory" - FILE "../../datas/factory.csv" OK.
Total lines in file: 42
Total lines in table: 42

TABLE "deftype" - FILE "../../datas/defaut.csv" OK.
Total lines in file: 45
Total lines in table: 45