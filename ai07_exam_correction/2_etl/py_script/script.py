# -*- coding: utf-8 -*-

# File Name     : script.py
# Description   : ETL script
#
# Requirements  : psycopg2, json, csv
#
# Version       : 1.0
# Created on    : Sat Mar  9 12:29:09 2019
#
# @author       : Thibault Santonja
# git depot     : https://gitlab.utc.fr/tsantonj/ai07
#
# =============================================================================

import psycopg2
import json

import etl
import log

HOST = 'tuxa.sme.utc'
DB = 'dbbdd1p002'
USER = 'bdd1p002'
PASSWD = input('enter the password: ')

# database connexion
def db_connexion():
    try:
        conn = psycopg2.connect(
                host =        HOST,
                dbname =    DB,
                user =         USER,
                password =     PASSWD
            )
        cursor = conn.cursor()

    except Exception as e:
        raise e

    return conn, cursor

# test request asked in the analyse
def test_request(conn, cursor):
    request = """
        SELECT d.label_cat, count(*) AS total
            FROM fbdl_exam.tickets t, fbdl_exam.defauts d
            WHERE t.fk_defaut = d.num_defaut
            GROUP BY d.label_cat
            ORDER BY d.label_cat ASC;
    """
    # execution
    cursor.execute(request)

    # get results in a python dictionnary (easier to analyse and to print)
    res = {}
    for row in cursor:
        res.update({row[0]: row[1]})

    # print results as a json (= python dictionnary)
    print('\nQuel est le nombre de defauts par categorie:')
    print(json.dumps(res, indent=2))



if __name__ == "__main__":
    # clean the log file and writing the file title.
    log_file = open('../log.md', 'w')
    log_file.write('log ETL\n===')
    log_file.close()

    # database connexion
    conn, cursor = db_connexion()

    # open the log file in 'a' mode and build each ETL zones
    log_file = open('../log.md', 'a')
    etl.e_zone(conn, cursor, log_file)
    etl.t_zone(conn, cursor, log_file)
    etl.l_zone(conn, cursor, log_file)

    # data control, comparaison between files and E zone, the rest of control
    # was already done before (with the creation of ETL zones)
    fo = '../../datas/'
    file_2_Ezone = {
        'cat': fo + 'categorie.csv',
        'deftype': fo + 'defaut.csv',
        'departement': fo + 'departement.csv',
        'factory': fo + 'factory.csv',
        'defaut': fo + 'Lawnmower00003.csv',
        'produit': fo + 'product.csv'
    }
    log.etl_control(conn, cursor, log_file, file_2_Ezone)

    # test the request given in the primary analyse
    test_request(conn, cursor)

    cursor.close()
    conn.close()
    log_file.close()

    print("\nETL done.\n0")
