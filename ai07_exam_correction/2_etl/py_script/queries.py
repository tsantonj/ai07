# -*- coding: utf-8 -*-

# File Name     : queries.py
# Description   : store all sql queries for extracting from source databases
#               and loading into the target database
#
# Requirements  :
#
# Version       : 1.0
# Created on    : Sat Mar  9 12:29:09 2019
#
# @author       : Thibault Santonja
# git depot     : https://gitlab.utc.fr/tsantonj/ai07
#
# =============================================================================


# ----------------------------------------------------------------------------
# ---------------------------- E zone SQL scripts ----------------------------
# ----------------------------------------------------------------------------

e_zone_drop = """DROP SCHEMA fbde_exam CASCADE;"""

e_zone_create = """
	CREATE SCHEMA fbde_exam
	  CREATE TABLE produit (
	    p_num text,
	    color text,
	    price text,
	    size text,
	    weight text,
	    d_age text
	  )

	  CREATE TABLE departement (
	    num text,
	    nom text,
	    pop text
	  )

	  CREATE TABLE defaut (
	    num text,
	    dt text,
	    prod text,
	    fact text,
	    type text
	  )

	  CREATE TABLE factory (
	    num text,
	    age text,
	    capa text,
	    loc Text
	  )

	    CREATE TABLE deftype (
	      type text,
	      cat text
	    )

	    CREATE TABLE cat (
	      cat text,
	      name text
	    )

	  GRANT ALL PRIVILEGES ON produit, departement, defaut, factory, deftype, cat TO bdd1p199;
"""

e_zone_insert_product = """COPY fbde_exam.produit FROM stdin WITH DELIMITER ';' CSV QUOTE '\"';"""
e_zone_insert_departement = """COPY fbde_exam.departement FROM stdin WITH DELIMITER ';' CSV QUOTE '\"';"""
e_zone_insert_defaut = """COPY fbde_exam.defaut FROM stdin WITH DELIMITER ';' CSV QUOTE '\"';"""
e_zone_insert_factory = """COPY fbde_exam.factory FROM stdin WITH DELIMITER ';' CSV QUOTE '\"';"""
e_zone_insert_deftype = """COPY fbde_exam.deftype FROM stdin WITH DELIMITER ';' CSV QUOTE '\"';"""
e_zone_insert_cat = """COPY fbde_exam.cat FROM stdin WITH DELIMITER ';' CSV QUOTE '\"';"""


# ----------------------------------------------------------------------------
# ---------------------------- T zone SQL scripts ----------------------------
# ----------------------------------------------------------------------------
t_zone_drop = """DROP SCHEMA fbdt_exam CASCADE;"""

t_zone_create = """
	CREATE SCHEMA fbdt_exam
	  CREATE TABLE produit (
	    p_num varchar(5),
	    color char(7),
	    price integer,
	    size integer,
	    weight integer,
	    d_age integer
	  )

	  CREATE TABLE departement (
	    num char(2),
	    nom text,
	    pop integer
	  )

	  CREATE TABLE defaut (
	    num varchar(11),
	    dt date,
	    prod varchar(5),
	    fact integer,
	    type integer
	  )

	  CREATE TABLE factory (
	    num integer,
	    age integer,
	    capa integer,
	    loc char(2)
	  )

	  CREATE TABLE deftype (
	    type integer,
	    cat char(1)
	  )

	  CREATE TABLE cat (
	    cat char(1),
	    name varchar(11)
	  )

	  GRANT ALL PRIVILEGES ON produit, departement, defaut, factory, deftype, cat TO bdd1p199;
"""

t_zone_insert = """
	INSERT INTO fbdt_exam.produit(p_num, color, price, size, weight, d_age)
	  SELECT CAST(p_num as varchar(5)), CAST(color as varchar(7)), CAST(price AS integer), CAST(size AS integer), CAST(weight as integer), CAST(d_age as integer)
	  FROM fbde_exam.produit;

	INSERT INTO fbdt_exam.departement(num, nom, pop)
	  SELECT CAST(num as char(2)), nom, CAST(pop as integer)
	  FROM fbde_exam.departement;

	INSERT INTO fbdt_exam.defaut(num, dt, prod, fact, type)
	  SELECT num, CAST(dt as date), prod, CAST(fact as integer), CAST(type as integer)
	  FROM fbde_exam.defaut;

	INSERT INTO fbdt_exam.factory(num, age, capa, loc)
	  SELECT CAST(num as integer), CAST(age as integer), CAST(capa as integer), CAST(loc as CHAR(2))
	  FROM fbde_exam.factory;

	INSERT INTO fbdt_exam.deftype(type, cat)
	  SELECT CAST(type as integer), cat
	  FROM fbde_exam.deftype;

	INSERT INTO fbdt_exam.cat(name, cat)
	  SELECT name, cat
	  FROM fbde_exam.cat;
"""


t_zone_functions = """
	CREATE OR REPLACE FUNCTION fbdt_exam.dateYear(date DATE) RETURNS INTEGER AS
	$$
	BEGIN
	    RETURN EXTRACT (YEAR FROM date);
	END;
	$$ LANGUAGE plpgsql;

	CREATE OR REPLACE FUNCTION fbdt_exam.dateSemester(date DATE) RETURNS INTEGER AS
	$$
	BEGIN
	    IF EXTRACT (QUARTER FROM date) = 1 OR EXTRACT (QUARTER FROM date) = 2 THEN
	      RETURN 1;
	    ELSE
	      RETURN 2;
	    END IF;
	END;
	$$ LANGUAGE plpgsql;

	CREATE OR REPLACE FUNCTION fbdt_exam.dateTrimester(date DATE) RETURNS INTEGER AS
	$$
	BEGIN
	    RETURN EXTRACT (QUARTER FROM date);
	END;
	$$ LANGUAGE plpgsql;

	CREATE OR REPLACE FUNCTION fbdt_exam.dateMonth(date DATE) RETURNS INTEGER AS
	$$
	BEGIN
	    RETURN EXTRACT (MONTH FROM date);
	END;
	$$ LANGUAGE plpgsql;

	CREATE OR REPLACE FUNCTION fbdt_exam.dateWeek(date DATE) RETURNS INTEGER AS
	$$
	BEGIN
	    RETURN EXTRACT (WEEK FROM date);
	END;
	$$ LANGUAGE plpgsql;

	CREATE OR REPLACE FUNCTION fbdt_exam.dateDayOfWeek(date DATE) RETURNS VARCHAR AS
	$$
	DECLARE
	  tmp INTEGER;
	BEGIN
	    tmp = EXTRACT (DOW FROM date);
	    CASE tmp
	      WHEN 1 THEN
	        RETURN 'Lundi';
	      WHEN 2 THEN
	        RETURN 'Mardi';
	      WHEN 3 THEN
	        RETURN 'Mercredi';
	      WHEN 4 THEN
	        RETURN 'Jeudi';
	      WHEN 5 THEN
	        RETURN 'Vendredi';
	      WHEN 6 THEN
	        RETURN 'Samedi';
	      ELSE
	        RETURN 'Dimanche';
	    END CASE;
	END;
	$$ LANGUAGE plpgsql;

	CREATE OR REPLACE FUNCTION fbdt_exam.topusine(numUsine integer) RETURNS integer AS
	$$
	DECLARE
	    tmp INTEGER;
	BEGIN
	  SELECT count(*) INTO tmp FROM fbdt_exam.vticket WHERE fact = numUsine;
	  IF tmp > 200 THEN
	    RETURN 1;
	  ELSE
	    RETURN 0;
	  END IF;
	END;
	$$ LANGUAGE plpgsql;

	CREATE OR REPLACE FUNCTION fbdt_exam.topdef(defType integer) RETURNS integer AS
	$$
	DECLARE
	    tmp INTEGER;
	BEGIN
	  SELECT count(*) INTO tmp FROM fbdt_exam.vticket WHERE type = defType;
	  IF tmp > 325 THEN
	    RETURN 1;
	  ELSE
	    RETURN 0;
	  END IF;
	END;
	$$ LANGUAGE plpgsql;
"""

t_zone_views = """
	CREATE TABLE fbdt_exam.tmp_dt AS
	  SELECT DISTINCT dt
	  FROM fbdt_exam.defaut;

	CREATE VIEW fbdt_exam.vDate (dt, annee, trimestre, mois, semaine, jourSemaine, jourAnnee) AS
	  SELECT dt, date_part('year', dt), date_part('quarter', dt), date_part('month', dt), date_part('week', dt), CASE
	      WHEN date_part('dow', dt) = 1 THEN 'Lundi'
	      WHEN date_part('dow', dt) = 2 THEN 'Mardi'
	      WHEN date_part('dow', dt) = 3 THEN 'Mercredi'
	      WHEN date_part('dow', dt) = 4 THEN 'Jeudi'
	      WHEN date_part('dow', dt) = 5 THEN 'Vendredi'
	      WHEN date_part('dow', dt) = 6 THEN 'Samedi'
	      WHEN date_part('dow', dt) = 0 THEN 'Dimanche'
	    END, date_part('DOY', dt)
	    FROM fbdt_exam.tmp_dt;

	CREATE VIEW fbdt_exam.vticket (num, dt, prod, fact, type) AS
	  SELECT num, dt, prod, fact, type
	    FROM fbdt_exam.defaut;
	select count(*) from fbdt_exam.vticket;

	CREATE VIEW fbdt_exam.vfactory (num, age, s_age, capa, loc, nom, pop, top_usine) AS
	  SELECT f.num, f.age,
	    CASE
	      WHEN f.age < 10 THEN 1
	      WHEN f.age < 20 AND f.age >= 10 THEN 2
	      WHEN f.age < 30 AND f.age >= 20 THEN 3
	      WHEN f.age < 40 AND f.age >= 30 THEN 4
	      WHEN f.age < 50 AND f.age >= 40 THEN 5
	      WHEN f.age >= 50 THEN 6
	    END, f.capa, f.loc, d.nom, d.pop, fbdt_exam.topusine(f.num)
	    FROM fbdt_exam.factory as f, fbdt_exam.departement as d
	    WHERE f.loc = d.num;
	select count(*) from fbdt_exam.vfactory;

	CREATE VIEW fbdt_exam.vproduit (num, color, price, size, weight, d_age, cat_age) AS
	  SELECT p_num, color, price, size, weight, d_age,
	    CASE
	      WHEN d_age < 5 THEN 1
	      WHEN d_age < 10 AND d_age >= 5 THEN 2
	      WHEN d_age < 15 AND d_age >= 10 THEN 3
	      WHEN d_age < 20 AND d_age >= 15 THEN 4
	      WHEN d_age < 25 AND d_age >= 20 THEN 5
	      WHEN d_age < 30 AND d_age >= 25 THEN 6
	      WHEN d_age < 35 AND d_age >= 30 THEN 7
	      WHEN d_age < 40 AND d_age >= 35 THEN 8
	      WHEN d_age >= 40 THEN 9
	    END
	    FROM fbdt_exam.produit;
	select count(*) from fbdt_exam.vproduit;

	CREATE VIEW fbdt_exam.vdefaut (type, cat, name, top_default) AS
	  SELECT d.type, d.cat, c.name, fbdt_exam.topdef(d.type)
	    FROM fbdt_exam.deftype as d, fbdt_exam.cat as c
	    WHERE d.cat = c.cat;
	select count(*) from fbdt_exam.vdefaut;
"""


# ----------------------------------------------------------------------------
# ---------------------------- L zone SQL scripts ----------------------------
# ----------------------------------------------------------------------------
l_zone_drop = """DROP SCHEMA fbdl_exam CASCADE;"""

l_zone_create = """
	CREATE SCHEMA fbdl_exam
	  CREATE TABLE dates (
	    date_full     date,
	    annee         integer,
	    trimestre     integer,
	    mois          integer,
	    semaine       integer,
	    jour_semaine  varchar(10),
	    jour_annee    integer
	  )

	  CREATE TABLE usines (
	    num_usine     integer,
	    age           integer,
	    capacite      integer,
	    localisation  char(2),
	    nom_dep       text,
	    pop_dep       integer,
	    cat_age       integer,
	    top_usine     integer
	  )

	  CREATE TABLE produits (
	    num_produit   varchar(5),
	    couleur       char(7),
	    prix          integer,
	    taille        integer,
	    poids         integer,
	    design_age    integer,
	    cat_age       integer
	  )

	  CREATE TABLE defauts (
	    num_defaut    integer,
	    categorie     char(1),
	    label_cat     varchar(11),
	    top_defaut    integer
	  )

	  CREATE TABLE tickets (
	    num_ticket    varchar(11),
	    fk_usine      integer,
	    fk_date       date,
	    fk_defaut     integer,
	    fk_produit    varchar(5),
	    quantity  integer
	  )

	  GRANT ALL PRIVILEGES ON dates, usines, produits, defauts, tickets TO bdd1p199;
"""

l_zone_insert = """
	INSERT INTO fbdl_exam.dates(date_full, annee, trimestre, mois, semaine, jour_semaine, jour_annee)
	  SELECT dt, annee, trimestre, mois, semaine, jourSemaine, jourAnnee
	  FROM fbdt_exam.vDate;

	INSERT INTO fbdl_exam.usines(num_usine, age, capacite, localisation, nom_dep, pop_dep, cat_age, top_usine)
	  SELECT num, age, capa, loc, nom, pop, s_age, top_usine
	  FROM fbdt_exam.vfactory;

	INSERT INTO fbdl_exam.produits(num_produit, couleur, prix, taille, poids, design_age, cat_age)
	  SELECT num, color, price, size, weight, d_age, cat_age
	  FROM fbdt_exam.vproduit;

	INSERT INTO fbdl_exam.defauts(num_defaut, categorie, label_cat, top_defaut)
	  SELECT type, cat, name, top_default
	  FROM fbdt_exam.vdefaut;

	INSERT INTO fbdl_exam.tickets(num_ticket, fk_date, fk_produit, fk_usine, fk_defaut)
	  SELECT num, dt, prod, fact, type
	  FROM fbdt_exam.vticket;
"""

l_zone_constraints = """
	ALTER TABLE fbdl_exam.defauts ADD CONSTRAINT cPkDefauts PRIMARY KEY(num_defaut);
	ALTER TABLE fbdl_exam.defauts ADD CONSTRAINT cCategorie CHECK (categorie IN ('A', 'B', 'C', 'D', 'E', 'F')  AND categorie IS NOT NULL);
	ALTER TABLE fbdl_exam.defauts ADD CONSTRAINT cLabel CHECK (label_cat IN ('Painting', 'Engine', 'Body', 'Electricity', 'Electronic', 'Equipment') AND label_cat IS NOT NULL);

	ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cPkProduits PRIMARY KEY(num_produit);
	ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cProduitNum CHECK(LEFT(num_produit, 3) = 'REF');
	ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cCouleur CHECK(LEFT(couleur, 1) = '#' AND couleur IS NOT NULL);
	ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cPrix CHECK(prix > 0 AND prix IS NOT NULL);
	ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cTaille CHECK(taille > 0 AND taille IS NOT NULL);
	ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cPoids CHECK(poids > 0 AND poids IS NOT NULL);
	ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cCatAgeProduit CHECK(cat_age IN (1,2,3,4,5,6,7,8,9) AND cat_age IS NOT NULL);

	ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cPkUsines PRIMARY KEY(num_usine);
	ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cCapacite CHECK(capacite > 0  AND capacite IS NOT NULL);
	ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cLocalisation CHECK(localisation IS NOT NULL);
	ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cNomDepartement CHECK(nom_dep IS NOT NULL);
	ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cPopulation CHECK(pop_dep > 0 AND pop_dep IS NOT NULL);
	ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cCatAgeUsine CHECK(cat_age IN (1,2,3,4,5,6) AND cat_age IS NOT NULL);

	ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cPkDates PRIMARY KEY (date_full);
	ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cAnnee CHECK (annee IS NOT NULL AND annee >= 2017 AND annee <= 2018);
	ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cTrimestre CHECK (trimestre IS NOT NULL AND trimestre IN (1, 2, 3, 4));
	ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cMois CHECK (mois IS NOT NULL AND mois >= 1 AND mois <= 12);
	ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cSemaine CHECK (semaine IS NOT NULL AND semaine >= 1 AND semaine <= 53);
	ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cJourSemaine CHECK (jour_semaine IS NOT NULL AND jour_semaine IN ('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'));
    ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cJourAnnee CHECK (jour_annee IS NOT NULL AND jour_annee >= 1 AND jour_annee <= 366);

	ALTER TABLE fbdl_exam.tickets ADD CONSTRAINT cFkUsine FOREIGN KEY (fk_usine) REFERENCES fbdl_exam.usines(num_usine);
	ALTER TABLE fbdl_exam.tickets ADD CONSTRAINT cFkDate FOREIGN KEY (fk_date) REFERENCES fbdl_exam.dates(date_full);
	ALTER TABLE fbdl_exam.tickets ADD CONSTRAINT cFkProduit FOREIGN KEY (fk_produit) REFERENCES fbdl_exam.produits(num_produit);
	ALTER TABLE fbdl_exam.tickets ADD CONSTRAINT cFkDefaut FOREIGN KEY (fk_defaut) REFERENCES fbdl_exam.defauts(num_defaut);
"""
