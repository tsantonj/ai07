# -*- coding: utf-8 -*-

# File Name     : log
# Description   : <enter description>
#
# Requirements  : csv
#
# Version       : 1.0
# Created on    : Wed Jun  5 11:13:16 2019
#
# @author       : Thibault Santonja
# git depot     :
#
# =============================================================================

import csv

# SQL query to get table name from a database schema
def get_tables(conn, cursor, schema):
    request = """
        SELECT table_name
            FROM information_schema.tables
            WHERE table_schema='""" + schema + """';
    """
    cursor.execute(request)

    res = []
    for row in cursor:
        res.append([row[0]])

    return res


# SQL query to count total lines in a table
def count_line(conn, cursor, schema, table):
    request = """SELECT COUNT(*) FROM """ + schema + """.""" + table[0] + """;"""
    cursor.execute(request)
    for row in cursor:
        return row[0]


# SQL query to get the 10 firsts lines of a table
def first_lines(conn, cursor, schema, table):
    request = """SELECT * FROM """ + schema + """.""" + table[0] + """ LIMIT 10;"""
    cursor.execute(request)

    line = '|'
    i = 0
    for column in cursor.description:
        line += '\t' + str(column[0]) + '\t|'
        i += 1

    res = [line]
    res.append('|---' * i + '|')

    for row in cursor:
        line = '|'
        for data in row:
            line += '\t' + str(data) + '\t|'
        res.append(line)

    return res


# get all tables of a schema and analyze all of these
def etl_log(conn, cursor, log_file, schema):
    tables = get_tables(conn, cursor, schema)
    log_file.write('\ntable list:' + str(tables))

    for table in tables:
        log_file.write('\n\n## Table ' + table[0])
        log_file.write('\ntotal lines: ' + str(count_line(conn, cursor, schema, table)))
        log_file.write('\n10 first lines: ')
        for line in first_lines(conn, cursor, schema, table):
            log_file.write('\n' + line)


# small function to print a text in the terminal and to write it in the log file
def print_log(txt, log_file):
    log_file.write('\n' + txt)
    print(txt)


# control the data import from files to E zone
def etl_control(conn, cursor, log_file, file_2_Ezone):
    log_file.write('\n\n\n# DATA CONTROL')

    for element in file_2_Ezone:
        table = element
        file = open(file_2_Ezone.get(element), 'r')
        file = csv.reader(file, delimiter=';', quotechar='"')
        nb_line_file = 0
        nb_line_db = count_line(conn, cursor, 'fbde_exam', [table])

        for line in file:
            nb_line_file += 1

        if nb_line_db != nb_line_file:
            log_file.write('\n\nERROR ON THE TABLE "' + table + '", AND THE FILE "' + file_2_Ezone.get(element) + '".')
        else:
            log_file.write('\n\nTABLE "' + table + '" - FILE "' + file_2_Ezone.get(element) + '" OK.')

        log_file.write('\nTotal lines in file: ' + str(nb_line_file))
        log_file.write('\nTotal lines in table: ' + str(nb_line_db))
    pass
