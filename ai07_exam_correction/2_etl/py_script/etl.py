# -*- coding: utf-8 -*-

# File Name     : etl
# Description   : <enter description>
#
# Requirements  :
#
# Version       : 1.0
# Created on    : Wed Jun  5 11:14:00 2019
#
# @author       : Thibault Santonja
# git depot     :
#
# =============================================================================

import log
import queries

# creation of the E zone of the ETL
def e_zone(conn, cursor, log_file):
    # create log file title for this zone
    log_file.write('\n\n\n# E zone')
    log.print_log('\n-= E zone =-', log_file)

    # execute sql queries to create table
    log.print_log('drop & create table...', log_file)
    cursor.execute(queries.e_zone_drop)
    cursor.execute(queries.e_zone_create)
    conn.commit()
    log.print_log("done !", log_file)

    # data recovering from .csv files
    log.print_log("insert datas...", log_file)
    cursor.copy_expert(sql=queries.e_zone_insert_product, file=open("../../datas/product.csv", "r"))
    cursor.copy_expert(sql=queries.e_zone_insert_departement, file=open("../../datas/departement.csv", "r"))
    cursor.copy_expert(sql=queries.e_zone_insert_defaut, file=open("../../datas/Lawnmower00003.csv", "r"))
    cursor.copy_expert(sql=queries.e_zone_insert_factory, file=open("../../datas/factory.csv", "r"))
    cursor.copy_expert(sql=queries.e_zone_insert_deftype, file=open("../../datas/defaut.csv", "r"))
    cursor.copy_expert(sql=queries.e_zone_insert_cat, file=open("../../datas/categorie.csv", "r"))
    conn.commit()
    log.print_log("done !", log_file)

    # tables control and write it in the log file
    log.etl_log(conn, cursor, log_file, 'fbde_exam')


# creation of the T zone of the ETL
def t_zone(conn, cursor, log_file):
    # create log file title for this zone
    log_file.write('\n\n\n# T zone')
    log.print_log("\n-= T zone =-", log_file)

    # execute sql queries to create table
    log.print_log("drop & create table...", log_file)
    cursor.execute(queries.t_zone_drop)
    cursor.execute(queries.t_zone_create)
    conn.commit()
    log.print_log("done !", log_file)

    # data insertion with SQL query calling
    log.print_log("insert datas...", log_file)
    cursor.execute(queries.t_zone_insert)
    log.print_log("done !", log_file)

    # execute T zone data transfomation, functions and views creation
    log.print_log("transform datas...", log_file)
    cursor.execute(queries.t_zone_functions)
    cursor.execute(queries.t_zone_views)
    conn.commit()
    log.print_log("done !", log_file)

    # tables control and write it in the log file
    log.etl_log(conn, cursor, log_file, 'fbdt_exam')


# creation of the L zone of the ETL
def l_zone(conn, cursor, log_file):
    # create log file title for this zone
    log_file.write('\n\n\n# L zone')
    log.print_log("\n-= L zone =-", log_file)

    # execute sql queries to create table
    log.print_log("drop & create table...", log_file)
    cursor.execute(queries.l_zone_drop)
    cursor.execute(queries.l_zone_create)
    conn.commit()
    log.print_log("done !", log_file)

    # data insertion with SQL query calling
    log.print_log("insert datas...", log_file)
    cursor.execute(queries.l_zone_insert)
    conn.commit()
    log.print_log("done !", log_file)

    # execute SQL queries to add some constraints to the tables
    log.print_log("constraint table...", log_file)
    cursor.execute(queries.l_zone_constraints)
    conn.commit()
    log.print_log("done !", log_file)

    # tables control and write it in the log file
    log.etl_log(conn, cursor, log_file, 'fbdl_exam')
