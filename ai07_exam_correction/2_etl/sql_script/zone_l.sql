-- File Name     : zone_l
-- Description   : ce fichier contient toutes les queries a executer.
--                 Ce sont les mêmes que celles présentes dans le fichier .py
--                 Ce fichier est juste présent pour les présenter.
-- @author       : Thibault Santonja
--------------------------------------

-- ETL ZONE L
DROP SCHEMA fbdl_exam CASCADE;

-- ZONE L CREATING
-- Table and schema creating
CREATE SCHEMA fbdl_exam
  CREATE TABLE dates (
    date_full     date,
    annee         integer,
    trimestre     integer,
    mois          integer,
    semaine       integer,
    jour_semaine  varchar(10),
    jour_annee    integer
  )

  CREATE TABLE usines (
    num_usine     integer,
    age           integer,
    capacite      integer,
    localisation  char(2),
    nom_dep       text,
    pop_dep       integer,
    cat_age       integer,
    top_usine     integer
  )

  CREATE TABLE produits (
    num_produit   varchar(5),
    couleur       char(7),
    prix          integer,
    taille        integer,
    poids         integer,
    design_age    integer,
    cat_age       integer
  )

  CREATE TABLE defauts (
    num_defaut    integer,
    categorie     char(1),
    label_cat     varchar(11),
    top_defaut    integer
  )

  CREATE TABLE tickets (
    num_ticket    varchar(11),
    fk_usine      integer,
    fk_date       date,
    fk_defaut     integer,
    fk_produit    varchar(5),
    quantity  integer
  )

  GRANT ALL PRIVILEGES ON dates, usines, produits, defauts, tickets TO bdd1p199;



-- IMPORT DES DONNEES DE LA ZONE T
INSERT INTO fbdl_exam.dates(date_full, annee, trimestre, mois, semaine, jour_semaine, jour_annee)
  SELECT dt, annee, trimestre, mois, semaine, jourSemaine, jourAnnee
  FROM fbdt_exam.vDate;
-- résultat: 668 imports => OK

INSERT INTO fbdl_exam.usines(num_usine, age, capacite, localisation, nom_dep, pop_dep, cat_age, top_usine)
  SELECT num, age, capa, loc, nom, pop, s_age, top_usine
  FROM fbdt_exam.vfactory;
-- résultat: 42 => OK

INSERT INTO fbdl_exam.produits(num_produit, couleur, prix, taille, poids, design_age, cat_age)
  SELECT num, color, price, size, weight, d_age, cat_age
  FROM fbdt_exam.vproduit;
-- résultat: 89 => OK

INSERT INTO fbdl_exam.defauts(num_defaut, categorie, label_cat, top_defaut)
  SELECT type, cat, name, top_default
  FROM fbdt_exam.vdefaut;
-- résultat: 45 => OK

INSERT INTO fbdl_exam.tickets(num_ticket, fk_date, fk_produit, fk_usine, fk_defaut)
  SELECT num, dt, prod, fact, type
  FROM fbdt_exam.vticket;
-- résultat: 4730 => OK



-- AJOUT DES CONTRAINTES
-- Table des faits defauts
ALTER TABLE fbdl_exam.defauts ADD CONSTRAINT cPkDefauts PRIMARY KEY(num_defaut);
ALTER TABLE fbdl_exam.defauts ADD CONSTRAINT cCategorie CHECK (categorie IN ('A', 'B', 'C', 'D', 'E', 'F')  AND categorie IS NOT NULL);
ALTER TABLE fbdl_exam.defauts ADD CONSTRAINT cLabel CHECK (label_cat IN ('Painting', 'Engine', 'Body', 'Electricity', 'Electronic', 'Equipment') AND label_cat IS NOT NULL);

-- Dimension produit
ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cPkProduits PRIMARY KEY(num_produit);
ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cProduitNum CHECK(LEFT(num_produit, 3) = 'REF');
ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cCouleur CHECK(LEFT(couleur, 1) = '#' AND couleur IS NOT NULL);
ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cPrix CHECK(prix > 0 AND prix IS NOT NULL);
ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cTaille CHECK(taille > 0 AND taille IS NOT NULL);
ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cPoids CHECK(poids > 0 AND poids IS NOT NULL);
ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cCatAgeProduit CHECK(cat_age IN (1,2,3,4,5,6,7,8,9) AND cat_age IS NOT NULL);

-- Dimension usine
ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cPkUsines PRIMARY KEY(num_usine);
ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cCapacite CHECK(capacite > 0  AND capacite IS NOT NULL);
ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cLocalisation CHECK(localisation IS NOT NULL);
ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cNomDepartement CHECK(nom_dep IS NOT NULL);
ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cPopulation CHECK(pop_dep > 0 AND pop_dep IS NOT NULL);
ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cCatAgeUsine CHECK(cat_age IN (1,2,3,4,5,6) AND cat_age IS NOT NULL);

--Dimension Date
ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cPkDates PRIMARY KEY (date_full);
ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cAnnee CHECK (annee IS NOT NULL AND annee >= 2017 AND annee <= 2018);
ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cTrimestre CHECK (trimestre IS NOT NULL AND trimestre IN (1, 2, 3, 4));
ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cMois CHECK (mois IS NOT NULL AND mois >= 1 AND mois <= 12);
ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cSemaine CHECK (semaine IS NOT NULL AND semaine >= 1 AND semaine <= 53);
ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cJourSemaine CHECK (jour_semaine IS NOT NULL AND jour_semaine IN ('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'));

-- Table des faits
ALTER TABLE fbdl_exam.tickets ADD CONSTRAINT cFkUsine FOREIGN KEY (fk_usine) REFERENCES fbdl_exam.usines(num_usine);
ALTER TABLE fbdl_exam.tickets ADD CONSTRAINT cFkDate FOREIGN KEY (fk_date) REFERENCES fbdl_exam.dates(date_full);
ALTER TABLE fbdl_exam.tickets ADD CONSTRAINT cFkProduit FOREIGN KEY (fk_produit) REFERENCES fbdl_exam.produits(num_produit);
ALTER TABLE fbdl_exam.tickets ADD CONSTRAINT cFkDefaut FOREIGN KEY (fk_defaut) REFERENCES fbdl_exam.defauts(num_defaut);



-- COMMAND WINDOW RESULTS:
--dbbdd1p002=> -- creation table et schema
--dbbdd1p002=> CREATE SCHEMA fbdl_exam
--dbbdd1p002->   CREATE TABLE dates (
--dbbdd1p002(>     date_full     date,
--dbbdd1p002(>     annee         integer,
--dbbdd1p002(>     trimestre     integer,
--dbbdd1p002(>     mois          integer,
--dbbdd1p002(>     semaine       integer,
--dbbdd1p002(>     jour_semaine  varchar(10)
--dbbdd1p002(>   )
--dbbdd1p002->
--dbbdd1p002->   CREATE TABLE usines (
--dbbdd1p002(>     num_usine     integer,
--dbbdd1p002(>     age           integer,
--dbbdd1p002(>     capacite      integer,
--dbbdd1p002(>     localisation  char(2),
--dbbdd1p002(>     nom_dep       text,
--dbbdd1p002(>     pop_dep       integer,
--dbbdd1p002(>     cat_age       integer
--dbbdd1p002(>   )
--dbbdd1p002->
--dbbdd1p002->   CREATE TABLE produits (
--dbbdd1p002(>     num_produit   varchar(5),
--dbbdd1p002(>     couleur       char(7),
--dbbdd1p002(>     prix          integer,
--dbbdd1p002(>     taille        integer,
--dbbdd1p002(>     poids         integer,
--dbbdd1p002(>     design_age    integer,
--dbbdd1p002(>     cat_age       integer
--dbbdd1p002(>   )
--dbbdd1p002->
--dbbdd1p002->   CREATE TABLE defauts (
--dbbdd1p002(>     num_defaut    integer,
--dbbdd1p002(>     categorie     char(1),
--dbbdd1p002(>     label_cat     varchar(11)
--dbbdd1p002(>   )
--dbbdd1p002->
--dbbdd1p002->   CREATE TABLE tickets (
--dbbdd1p002(>     num_ticket    varchar(11),
--dbbdd1p002(>     fk_usine      integer,
--dbbdd1p002(>     fk_date       date,
--dbbdd1p002(>     fk_defaut     integer,
--dbbdd1p002(>     fk_produit    varchar(5),
--dbbdd1p002(>     quantity  integer
--dbbdd1p002(>   )
--dbbdd1p002->
--dbbdd1p002->   GRANT ALL PRIVILEGES ON dates, usines, produits, defauts, tickets TO bdd1p199;
--CREATE SCHEMA
--dbbdd1p002=>                                                     ^
--dbbdd1p002=> INSERT INTO fbdl_exam.dates(date_full, annee, trimestre, mois, semaine, jour_semaine)
--dbbdd1p002->   SELECT dt, annee, trimestre, mois, semaine, jourSemaine
--dbbdd1p002->   FROM fbdt_exam.vDate;
--INSERT 0 668
--dbbdd1p002=> INSERT INTO fbdl_exam.usines(num_usine, age, capacite, localisation, nom_dep, pop_dep, cat_age)
--dbbdd1p002->   SELECT num, age, capa, loc, nom, pop, s_age
--dbbdd1p002->   FROM fbdt_exam.vfactory;
--INSERT 0 42                                    ^
--dbbdd1p002=> INSERT INTO fbdl_exam.produits(num_produit, couleur, prix, taille, poids, design_age, cat_age)
--dbbdd1p002->   SELECT num, color, price, size, weight, d_age, cat_age
--dbbdd1p002->   FROM fbdt_exam.vproduit;
--INSERT 0 89
--dbbdd1p002=> INSERT INTO fbdl_exam.defauts(num_defaut, categorie, label_cat)
--dbbdd1p002->   SELECT type, cat, name
--dbbdd1p002->   FROM fbdt_exam.vdefaut;
--INSERT 0 45
--dbbdd1p002=> INSERT INTO fbdl_exam.tickets(num_ticket, fk_date, fk_produit, fk_usine, fk_defaut)
--dbbdd1p002->   SELECT num, dt, prod, fact, type
--dbbdd1p002->   FROM fbdt_exam.vticket;
--INSERT 0 4730
--dbbdd1p002=>
--dbbdd1p002=>   -- Dimension défauts
--dbbdd1p002=> ALTER TABLE fbdl_exam.defauts ADD CONSTRAINT cPkDefauts PRIMARY KEY(num_defaut);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.defauts ADD CONSTRAINT cCategorie CHECK (categorie IN ('A', 'B', 'C', 'D', 'E', 'F')  AND categorie IS NOT NULL);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.defauts ADD CONSTRAINT cLabel CHECK (label_cat IN ('Painting', 'Engine', 'Body', 'Electricity', 'Electronic', 'Equipment') AND label_cat IS NOT NULL);
--ALTER TABLE
--dbbdd1p002=>
--dbbdd1p002=> -- Dimension produit
--dbbdd1p002=> ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cPkProduits PRIMARY KEY(num_produit);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cProduitNum CHECK(LEFT(num_produit, 3) = 'REF');
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cCouleur CHECK(LEFT(couleur, 1) = '#' AND couleur IS NOT NULL);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cPrix CHECK(prix > 0 AND prix IS NOT NULL);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cTaille CHECK(taille > 0 AND taille IS NOT NULL);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cPoids CHECK(poids > 0 AND poids IS NOT NULL);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.produits ADD CONSTRAINT cCatAgeProduit CHECK(cat_age IN (1,2,3,4,5,6,7,8,9) AND cat_age IS NOT NULL);
--ALTER TABLE
--dbbdd1p002=>
--dbbdd1p002=> -- Dimension usine
--dbbdd1p002=> ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cPkUsines PRIMARY KEY(num_usine);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cCapacite CHECK(capacite > 0  AND capacite IS NOT NULL);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cLocalisation CHECK(localisation IS NOT NULL);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cNomDepartement CHECK(nom_dep IS NOT NULL);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cPopulation CHECK(pop_dep > 0 AND pop_dep IS NOT NULL);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.usines ADD CONSTRAINT cCatAgeUsine CHECK(cat_age IN (1,2,3,4,5,6) AND cat_age IS NOT NULL);
--ALTER TABLE
--dbbdd1p002=>
--dbbdd1p002=> --Dimension Date
--dbbdd1p002=> ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cPkDates PRIMARY KEY (date_full);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cAnnee CHECK (annee IS NOT NULL AND annee >= 2017 AND annee <= 2018);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cTrimestre CHECK (trimestre IS NOT NULL AND trimestre IN (1, 2, 3, 4));
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cMois CHECK (mois IS NOT NULL AND mois >= 1 AND mois <= 12);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cSemaine CHECK (semaine IS NOT NULL AND semaine >= 1 AND semaine <= 53);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.dates ADD CONSTRAINT cJourSemaine CHECK (jour_semaine IS NOT NULL AND jour_semaine IN ('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'));
--ALTER TABLE
--dbbdd1p002=>
--dbbdd1p002=> -- Table des faits
--dbbdd1p002=> ALTER TABLE fbdl_exam.tickets ADD CONSTRAINT cFkUsine FOREIGN KEY (fk_usine) REFERENCES fbdl_exam.usines(num_usine);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.tickets ADD CONSTRAINT cFkDate FOREIGN KEY (fk_date) REFERENCES fbdl_exam.dates(date_full);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.tickets ADD CONSTRAINT cFkProduit FOREIGN KEY (fk_produit) REFERENCES fbdl_exam.produits(num_produit);
--ALTER TABLE
--dbbdd1p002=> ALTER TABLE fbdl_exam.tickets ADD CONSTRAINT cFkDefaut FOREIGN KEY (fk_defaut) REFERENCES fbdl_exam.defauts(num_defaut);
--ALTER TABLE
