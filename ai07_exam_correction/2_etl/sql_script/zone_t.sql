-- File Name     : zone_t
-- Description   : ce fichier contient toutes les queries a executer.
--                 Ce sont les mêmes que celles présentes dans le fichier .py
--                 Ce fichier est juste présent pour les présenter.
-- @author       : Thibault Santonja
--------------------------------------

-- ETL ZONE T
DROP SCHEMA fbdt_exam CASCADE;

-- ZONE T CREATING
-- Table and schema creating
CREATE SCHEMA fbdt_exam
  CREATE TABLE produit (
    p_num varchar(5),
    color char(7),
    price integer,
    size integer,
    weight integer,
    d_age integer
  )

  CREATE TABLE departement (
    num char(2),
    nom text,
    pop integer
  )

  CREATE TABLE defaut (
    num varchar(11),
    dt date,
    prod varchar(5),
    fact integer,
    type integer
  )

  CREATE TABLE factory (
    num integer,
    age integer,
    capa integer,
    loc char(2)
  )

  CREATE TABLE deftype (
    type integer,
    cat char(1)
  )

  CREATE TABLE cat (
    cat char(1),
    name varchar(11)
  )

  GRANT ALL PRIVILEGES ON produit, departement, defaut, factory, deftype, cat TO bdd1p199;


-- INSERTION DANS LA ZONE T
-- Insertion des produits
INSERT INTO fbdt_exam.produit(p_num, color, price, size, weight, d_age)
  SELECT CAST(p_num as varchar(5)), CAST(color as varchar(7)), CAST(price AS integer), CAST(size AS integer), CAST(weight as integer), CAST(d_age as integer)
  FROM fbde_exam.produit;
-- Insertion des départements
INSERT INTO fbdt_exam.departement(num, nom, pop)
  SELECT CAST(num as char(2)), nom, CAST(pop as integer)
  FROM fbde_exam.departement;
-- Insertion des défauts
INSERT INTO fbdt_exam.defaut(num, dt, prod, fact, type)
  SELECT num, CAST(dt as date), prod, CAST(fact as integer), CAST(type as integer)
  FROM fbde_exam.defaut;
-- Insertion des usines
INSERT INTO fbdt_exam.factory(num, age, capa, loc)
  SELECT CAST(num as integer), CAST(age as integer), CAST(capa as integer), CAST(loc as CHAR(2))
  FROM fbde_exam.factory;
-- Insertion des types de defaut
INSERT INTO fbdt_exam.deftype(type, cat)
  SELECT CAST(type as integer), cat
  FROM fbde_exam.deftype;
-- Insertion des catégories de defaut
INSERT INTO fbdt_exam.cat(name, cat)
  SELECT name, cat
  FROM fbde_exam.cat;




-- FONCTION DE TRAITEMENT DES DATES
-- Récupération de l'année
CREATE OR REPLACE FUNCTION fbdt_exam.dateYear(date DATE) RETURNS INTEGER AS
$$
BEGIN
    RETURN EXTRACT (YEAR FROM date);
END;
$$ LANGUAGE plpgsql;

-- Récupération du semestre
CREATE OR REPLACE FUNCTION fbdt_exam.dateSemester(date DATE) RETURNS INTEGER AS
$$
BEGIN
    IF EXTRACT (QUARTER FROM date) = 1 OR EXTRACT (QUARTER FROM date) = 2 THEN
      RETURN 1;
    ELSE
      RETURN 2;
    END IF;
END;
$$ LANGUAGE plpgsql;


-- récupération du trimestre
CREATE OR REPLACE FUNCTION fbdt_exam.dateTrimester(date DATE) RETURNS INTEGER AS
$$
BEGIN
    RETURN EXTRACT (QUARTER FROM date);
END;
$$ LANGUAGE plpgsql;


-- Récupération du mois
CREATE OR REPLACE FUNCTION fbdt_exam.dateMonth(date DATE) RETURNS INTEGER AS
$$
BEGIN
    RETURN EXTRACT (MONTH FROM date);
END;
$$ LANGUAGE plpgsql;


-- Récupération de la semaine
CREATE OR REPLACE FUNCTION fbdt_exam.dateWeek(date DATE) RETURNS INTEGER AS
$$
BEGIN
    RETURN EXTRACT (WEEK FROM date);
END;
$$ LANGUAGE plpgsql;


-- Récupération du label du jour
CREATE OR REPLACE FUNCTION fbdt_exam.dateDayOfWeek(date DATE) RETURNS VARCHAR AS
$$
DECLARE
  tmp INTEGER;
BEGIN
    tmp = EXTRACT (DOW FROM date);
    CASE tmp
      WHEN 1 THEN
        RETURN 'Lundi';
      WHEN 2 THEN
        RETURN 'Mardi';
      WHEN 3 THEN
        RETURN 'Mercredi';
      WHEN 4 THEN
        RETURN 'Jeudi';
      WHEN 5 THEN
        RETURN 'Vendredi';
      WHEN 6 THEN
        RETURN 'Samedi';
      ELSE
        RETURN 'Dimanche';
    END CASE;
END;
$$ LANGUAGE plpgsql;


-- FONCTIONS COMPLEMENTAIRES
-- Récupération de l'usine traitant le plus de défauts
CREATE OR REPLACE FUNCTION fbdt_exam.topusine(numUsine integer) RETURNS integer AS
$$
DECLARE
    tmp INTEGER;
BEGIN
  SELECT count(*) INTO tmp FROM fbdt_exam.vticket WHERE fact = numUsine;
  IF tmp > 200 THEN
    RETURN 1;
  ELSE
    RETURN 0;
  END IF;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION fbdt_exam.topdef(defType integer) RETURNS integer AS
$$
DECLARE
    tmp INTEGER;
BEGIN
  SELECT count(*) INTO tmp FROM fbdt_exam.vticket WHERE type = defType;
  IF tmp > 1000 THEN
    RETURN 1;
  ELSE
    RETURN 0;
  END IF;
END;
$$ LANGUAGE plpgsql;



-- CREATION DES VUES
-- table temporaire pour recuperer toutes les dates
CREATE TABLE fbdt_exam.tmp_dt AS
  SELECT DISTINCT dt
  FROM fbdt_exam.defaut;

-- vue pour la future table de dates
CREATE VIEW fbdt_exam.vDate (dt, annee, trimestre, mois, semaine, jourSemaine, jourAnnee) AS
  SELECT dt, date_part('year', dt), date_part('quarter', dt), date_part('month', dt), date_part('week', dt), CASE
      WHEN date_part('dow', dt) = 1 THEN 'Lundi'
      WHEN date_part('dow', dt) = 2 THEN 'Mardi'
      WHEN date_part('dow', dt) = 3 THEN 'Mercredi'
      WHEN date_part('dow', dt) = 4 THEN 'Jeudi'
      WHEN date_part('dow', dt) = 5 THEN 'Vendredi'
      WHEN date_part('dow', dt) = 6 THEN 'Samedi'
      WHEN date_part('dow', dt) = 0 THEN 'Dimanche'
    END, DATE_FORMAT(dt, '%j')
    FROM fbdt_exam.tmp_dt;
-- résultat: 668 imports => OK

--vue pour la future table ticket
CREATE VIEW fbdt_exam.vticket (num, dt, prod, fact, type) AS
  SELECT num, dt, prod, fact, type
    FROM fbdt_exam.defaut;
select count(*) from fbdt_exam.vticket;
-- résultat: 4730 => OK



--vue pour la future table factory
CREATE VIEW fbdt_exam.vfactory (num, age, s_age, capa, loc, nom, pop, top_usine) AS
  SELECT f.num, f.age,
    CASE
      WHEN f.age < 10 THEN 1
      WHEN f.age < 20 AND f.age >= 10 THEN 2
      WHEN f.age < 30 AND f.age >= 20 THEN 3
      WHEN f.age < 40 AND f.age >= 30 THEN 4
      WHEN f.age < 50 AND f.age >= 40 THEN 5
      WHEN f.age >= 50 THEN 6
    END, f.capa, f.loc, d.nom, d.pop, fbdt_exam.topusine(f.num)
    FROM fbdt_exam.factory as f, fbdt_exam.departement as d
    WHERE f.loc = d.num;
select count(*) from fbdt_exam.vfactory;
-- résultat: 42 => OK


-- vue pour la future table produit
CREATE VIEW fbdt_exam.vproduit (num, color, price, size, weight, d_age, cat_age) AS
  SELECT p_num, color, price, size, weight, d_age,
    CASE
      WHEN d_age < 5 THEN 1
      WHEN d_age < 10 AND d_age >= 5 THEN 2
      WHEN d_age < 15 AND d_age >= 10 THEN 3
      WHEN d_age < 20 AND d_age >= 15 THEN 4
      WHEN d_age < 25 AND d_age >= 20 THEN 5
      WHEN d_age < 30 AND d_age >= 25 THEN 6
      WHEN d_age < 35 AND d_age >= 30 THEN 7
      WHEN d_age < 40 AND d_age >= 35 THEN 8
      WHEN d_age >= 40 THEN 9
    END
    FROM fbdt_exam.produit;
select count(*) from fbdt_exam.vproduit;
-- résultat: 89 => OK



CREATE VIEW fbdt_exam.vdefaut (type, cat, name, top_default) AS
  SELECT d.type, d.cat, c.name, fbdt_exam.topdef(d.type)
    FROM fbdt_exam.deftype as d, fbdt_exam.cat as c
    WHERE d.cat = c.cat;
select count(*) from fbdt_exam.vdefaut;
-- résultat: 45 => OK
