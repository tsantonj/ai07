-- File Name     : zone_e
-- Description   : ce fichier contient toutes les queries a executer.
--                 Ce sont les mêmes que celles présentes dans le fichier .py
--                 Ce fichier est juste présent pour les présenter.
-- @author       : Thibault Santonja
--------------------------------------

-- ETL ZONE E
DROP SCHEMA fbde_exam CASCADE;

-- ZONE E CREATING
-- Table and schema creating
CREATE SCHEMA fbde_exam
  CREATE TABLE produit (
    p_num text,
    color text,
    price text,
    size text,
    weight text,
    d_age text
  )

  CREATE TABLE departement (
    num text,
    nom text,
    pop text
  )

  CREATE TABLE defaut (
    num text,
    dt text,
    prod text,
    fact text,
    type text
  )

  CREATE TABLE factory (
    num text,
    age text,
    capa text,
    loc Text
  )

    CREATE TABLE deftype (
      type text,
      cat text
    )

    CREATE TABLE cat (
      cat text,
      name text
    )

  GRANT ALL PRIVILEGES ON produit, departement, defaut, factory, deftype, cat TO bdd1p199;


-- CSV IMPORT
\copy fbde_exam.produit FROM '~/Documents/ai07/ai07_exam_correction/datas/Product' DELIMITER ';' CSV;
\copy fbde_exam.departement FROM '~/Documents/ai07/ai07_exam_correction/datas/dpt_fr' DELIMITER ';' CSV;
\copy fbde_exam.defaut FROM '~/Documents/ai07/ai07_exam_correction/datas/Lawnmower00003' DELIMITER ';' CSV;
\copy fbde_exam.factory FROM '~/Documents/ai07/ai07_exam_correction/datas/Factory' DELIMITER ';' CSV;
\copy fbde_exam.deftype FROM '~/Documents/ai07/ai07_exam_correction/datas/DefaultType' DELIMITER ';' CSV;
\copy fbde_exam.cat FROM '~/Documents/ai07/ai07_exam_correction/datas/Category' DELIMITER ';' CSV;


-- COMMAND WINDOW RESULTS:
--dbbdd1p002=> \copy fbde_exam.produit FROM '~/Documents/ai07/ai07_exam_correction/datas/product.csv' DELIMITER ';' CSV;
--COPY 89
--dbbdd1p002=> \copy fbde_exam.departement FROM '~/Documents/ai07/ai07_exam_correction/datas/departement.csv' DELIMITER ';' CSV;
--COPY 95
--dbbdd1p002=> \copy fbde_exam.defaut FROM '~/Documents/ai07/ai07_exam_correction/datas/Lawnmower00003.csv' DELIMITER ';' CSV;
--COPY 4730
--dbbdd1p002=> \copy fbde_exam.factory FROM '~/Documents/ai07/ai07_exam_correction/datas/factory.csv' DELIMITER ';' CSV;
--COPY 42
--dbbdd1p002=> \copy fbde_exam.deftype FROM '~/Documents/ai07/ai07_exam_correction/datas/defaut.csv' DELIMITER ';' CSV;
--COPY 45
--dbbdd1p002=> \copy fbde_exam.cat FROM '~/Documents/ai07/ai07_exam_correction/datas/categorie.csv' DELIMITER ';' CSV;
--COPY 6
