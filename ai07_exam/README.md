ai07_exam
===

# Généralités
## Contenu
**DOSSIERS**
- dossier `datas` contient les fichiers téléchargés
- dossier `etl` contient 4 scripts SQL, un pour chaque zone de l'ETL et unpour les requêtes sur le DW
- dossier `log_DW` contient le résultat de la requête `SELECT * FROM <fait_ou_dimension>`, pour chauqe table du datawarehouse
- dossier `mod contient` les choix de modélisation pour le datawarehouse
- dossier `python_etl_script` contient des scripts python afin de créer le data warehouse

**FICHIERS**
- `consignes.md` sont les consignes de l'exam
- `log.md` est le résultat de la dernière execution des scripts permettant de créer l'ETL
- `queries.md` sont les queries réalisées le mardi 2 avril entre 8 h et 10 h


## Script pour la création de l'ETL
Librairies utilisées :
- `csv` permettant de travailler avec des fichiers CSV
- `pandas` suite d'outils afin de travailler et traiter rapidement des données (utilisation des Dataframes)
- `wget` permet d'utiliser la commande terminal 'wget' permettant de recuperer un fichier d'internet
- `psycopg2` permet d'utiliser des scripts SQL en python
- `sys` utile pour les `sys.exit(error)` afin de prévenir d'une erreur

4 fichiers python sont necessaires:
- main, contenant le code python à executer. Il contient tous les appels pour créer les différentes zones, les copies, les contrôles et la création d'un fichier log.md. Une optimmisation de la structuration du code consisterait à sortir les code de création des zones et les mettre respectivement dans des fichiers "zone_E.py", "zone_T.py" et "zone_L.py"
- sql_queries, contenant les queries SQL
- variables, contenant les variable globale au projet (host de la base de donnée par exemple)
- db_credentials, contenant des dictionnaires de données décrivant les base de données où l'on récupère les données via la zone E

**EDIT** suite à un problème incompréhensible de `\copy`, les script python n'ont pas été utilisés, bien que tout le reste fonctionne. La copy des csv dans la zone E étant impossible, j'ai décidé d'utiliser un script en bash. Contenant mes identifiants de connexion à l'ordinateur, ce script n'est pas sur git.


# Auteur
**id**: bdd1p002
**nom**: Thibault Santonja
