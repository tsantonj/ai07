--------------------------------------------------------------------------------
----------------------------- REPONSE AUX QUESTIONS ----------------------------
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- Question 1 : Analyser les faits en fonction des catÃ©gories de dÃ©faut

CREATE VIEW nbdefautscategorie AS
  SELECT d.label_cat, count(*) AS total
    FROM fbdl_exam.tickets t, fbdl_exam.defauts d
    WHERE t.fk_defaut = d.num_defaut
    GROUP BY d.label_cat
    ORDER BY d.label_cat ASC;
SELECT * FROM nbdefautscategorie;


label_cat  | total
-------------+-------
Body        |   205
Electricity |   223
Electronic  |   342
Engine      |   289
Equipment   |   385
Painting    |  3286
(6 lignes)

-- On peut vite voir qu'une majorité des défauts sont des défauts de painture.
-- Il serait intéressant d'étudier le couts de ces différents défauts afin de
-- relever lesquels doivent être le plus pris en compte. Un défaut de peinture
-- est surement moins grave et couteux que un défaut mécanique plus interne.
-- Exemple un moteur de voiture est rare mais couteux comparé à une rayure.



--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- Question 2 : Ã‰tudier les faits en fonction du jour de l'annÃ©e.

DROP VIEW defautjour;
CREATE VIEW defautjour AS
  SELECT d.date_full AS dates, COUNT(*) AS Tickets
    FROM fbdl_exam.dates d, fbdl_exam.tickets t
    WHERE d.date_full = t.fk_date
    GROUP BY dates
    ORDER BY dates ASC;
SELECT * FROM defautjour;

-- cette requète est intéressante à analyser dans un graphique.
-- nous allons donc étudier d'autres grains de segmentation pour avoir une analyse
-- plus rapide et parlante, par jour, mois et trimestre:

CREATE VIEW defautjds AS
  SELECT d.jour_semaine AS JDS, COUNT(*) AS Tickets
    FROM fbdl_exam.dates d, fbdl_exam.tickets t
    WHERE d.date_full = t.fk_date
    GROUP BY d.jour_semaine
    ORDER BY d.jour_semaine='Lundi', d.jour_semaine='Mardi',
      d.jour_semaine='Mercredi', d.jour_semaine='Jeudi', d.jour_semaine='Vendredi',
      d.jour_semaine='Samedi', d.jour_semaine='Dimanche';
SELECT * FROM defautjds;


jds    | tickets
----------+--------
Dimanche |    555
Samedi   |    764
Vendredi |    859
Jeudi    |    675
Mercredi |    690
Mardi    |    660
Lundi    |    527
(7 lignes)
-- on peut voir qu'il y a plus de défaut analysés les vendredis
-- il serait intéressant de voir l'effectif par jour et le nombre d'heures
-- travaillés par jour pour s'assurer qu'il n'y en ai pas plus certains jours


CREATE VIEW defautmonth AS
  SELECT d.mois AS month, COUNT(*) AS Tickets
    FROM fbdl_exam.dates d, fbdl_exam.tickets t
    WHERE d.date_full = t.fk_date
    GROUP BY month
    ORDER BY month ASC;
SELECT * FROM defautmonth;


 month | tickets
-------+--------
     1 |    289
     2 |    386
     3 |    779
     4 |    425
     5 |    386
     6 |    539
     7 |    263
     8 |    400
     9 |    436
    10 |    240
    11 |    324
    12 |    263
(12 lignes)
-- on peut voir qu'il y a plus de défaut analysés en mars
-- il serait intéressant de voir l'effectif par jour et le nombre d'heures
-- travaillés par mois pour s'assurer qu'il n'y en ai pas plus suivant le mois

CREATE VIEW defauttrim AS
  SELECT d.trimestre AS trimestre, COUNT(*) AS Tickets
    FROM fbdl_exam.dates d, fbdl_exam.tickets t
    WHERE d.date_full = t.fk_date
    GROUP BY trimestre
    ORDER BY trimestre ASC;
SELECT * FROM defauttrim;


trimestre | tickets
-----------+---------
        1 |    1454
        2 |    1350
        3 |    1099
        4 |     827
(4 lignes)
-- on peut voir qu'il y a plus de défaut analysés en début d'année
-- il serait intéressant de voir l'effectif par jour et le nombre d'heures
-- travaillés par mois pour s'assurer qu'il n'y en ai pas plus suivant le mois
-- on peut citer les vacances hivernales réduisant souvent l'activité d'une
-- entreprise et décalant son travail sur le début de l'année suivante.

-- il serait intéresant d'analyser le tout par rapport au temps de travail total
-- par jour, mois ou trimestre. Si un mois à vu plus d'heure de travail au total
-- (plus d'heures supplémentaire, plus d'effectif, ...) il est normal que plus
-- de défauts on été analysés



--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- Question 3 : Analyser les faits par rapport aux ages des produits
-- analyse par rapport à l'age
CREATE VIEW nbdefautsage AS
  SELECT p.design_age AS age, count(*) AS total
    FROM fbdl_exam.tickets t, fbdl_exam.produits p
    WHERE t.fk_produit = p.num_produit
    GROUP BY p.design_age
    ORDER BY p.design_age ASC;
SELECT * FROM nbdefautsage;

-- analyse par rapport à la tranche d'age
-- rappel des categories:
-- - 1 : [0;4]
-- - 2 : [5;9]
-- - 3 : [10;14]
-- - 4 : [15;19]
-- - 5 : [20;25]
-- - 6 : [25;29]
-- - 7 : [30;34]
-- - 8 : [35; 39]
-- - 9 : [40+]
CREATE VIEW nbdefautstrancheage AS
  SELECT p.cat_age AS tranche_d_age, count(*) AS total
    FROM fbdl_exam.tickets t, fbdl_exam.produits p
    WHERE t.fk_produit = p.num_produit
    GROUP BY tranche_d_age
    ORDER BY tranche_d_age ASC;
SELECT * FROM nbdefautstrancheage;

tranche_d_age | total
---------------+-------
            1 |   603
            2 |   222
            3 |   811
            4 |   596
            5 |   608
            6 |   724
            7 |   681
            8 |   319
            9 |   166
(9 lignes)
-- on peut voir qui on entre 10 et 14 ans sont ceux ayant le plus de défauts.
-- il serait intéressant d'analyser le nombre de produit existant par tranche
-- d'age.
CREATE VIEW tot_produit_p_age AS
  SELECT count(*) AS totalProduit, p.cat_age as P_t_age
    FROM fbdl_exam.produits p
    GROUP BY p.cat_age
    ORDER BY p.cat_age ASC;

CREATE VIEW rapport_defaut_produit AS
  SELECT d.tranche_d_age as tranche_age, d.total as total_defauts,
    t.totalProduit as total_produit, d.total/t.totalProduit as rapport_defaut_produit
    FROM nbdefautstrancheage d, tot_produit_p_age t
    WHERE d.tranche_d_age = t.P_t_age
    ORDER BY d.tranche_d_age ASC;
SELECT * FROM rapport_defaut_produit;

tranche_age | total_defauts | total_produit | rapport_defaut_produit
-------------+---------------+---------------+------------------------
          1 |           603 |             2 |                    301
          2 |           222 |             1 |                    222
          3 |           811 |            10 |                     81
          4 |           596 |            10 |                     59
          5 |           608 |            11 |                     55
          6 |           724 |            18 |                     40
          7 |           681 |            19 |                     35
          8 |           319 |            11 |                     29
          9 |           166 |             7 |                     23
(9 lignes)

-- on observe que les produits récents ont plus de défauts que les anciens.
-- nous avons pour les produits de moins de 5 ans, environ 301 défauts par produit.
-- cependant cette analyse n'est pas encore juste. il faudrait comparer ce
-- résultat avec le nombre de produit en circulation. on peut supposer que les
-- produits récents sont plus présents que les anciens, expliquant en partie de
-- ce fait la surprésence de défauts sur les produits de moins de 10 ans, comparé
-- à ceux de plus de 10 ans.



--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- Question 4 : Analyser les faits en fonction de la capacitÃ© des usines
CREATE VIEW defautusinecapac AS
  SELECT u.capacite, count(*) AS totalDefauts
    FROM fbdl_exam.usines u, fbdl_exam.tickets t
    WHERE t.fk_usine = u.num_usine
    GROUP BY u.capacite
    ORDER BY u.capacite ASC;
SELECT * FROM defautusinecapac;

capacite | totaldefauts
----------+--------------
       2 |        14190
       3 |        28380
       4 |        28380
       5 |        28380
       6 |        33110
       7 |        28380
       8 |        28380
       9 |         9460
(8 lignes)

-- on peut voir une assez grande continuité suivant la capacité, si l'on enlève
-- les capacités de 2 ou 9.

-- comparons maintenant avec le nombre d'usines ayant ces capacité

CREATE VIEW tot_usine_p_capacite AS
  SELECT count(*) AS totalUsines, u.capacite
    FROM fbdl_exam.usines u
    GROUP BY u.capacite
    ORDER BY u.capacite ASC;

CREATE VIEW rapport_defaut_usine AS
  SELECT d.capacite, d.totalDefauts, t.totalUsines,
    d.totalDefauts/t.totalUsines as rapport_defaut_usine
    FROM defautusinecapac d, tot_usine_p_capacite t
    WHERE d.capacite = t.capacite
    ORDER BY d.capacite ASC;
SELECT * FROM rapport_defaut_usine;

capacite | totaldefauts | totalusines | rapport_defaut_usine
----------+--------------+-------------+----------------------
       2 |        14190 |           3 |                 4730
       3 |        28380 |           6 |                 4730
       4 |        28380 |           6 |                 4730
       5 |        28380 |           6 |                 4730
       6 |        33110 |           7 |                 4730
       7 |        28380 |           6 |                 4730
       8 |        28380 |           6 |                 4730
       9 |         9460 |           2 |                 4730
(8 lignes)

-- on peut voir au final que le nombre de défaut par usine suivant la capacité
-- est constant.








--------------------------------------------------------------------------------
-------------------------- QUESTIONS SUPPLEMENTAIRES ---------------------------
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- Analyse du  nombres de défauts par produit

CREATE VIEW nbdefautsproduit AS
  SELECT p.num_produit, p.prix, COUNT(*) AS Pb
    FROM fbdl_exam.produits p, fbdl_exam.tickets t
    WHERE t.fk_produit = p.num_produit
    GROUP BY p.num_produit
    ORDER BY Pb DESC;
SELECT * FROM nbdefautsproduit;

-- réduction de l'affichage aux 10 premiers et derniers
num_produit | prix | pb
-------------+------+-----
 REF36       |   29 | 383
 REF52       |  513 | 222
 REF39       | 1110 | 220
 REF19       | 1621 | 112
 REF24       |  499 | 108
 REF73       |  175 |  94
 REF43       |  397 |  86
 REF30       | 1055 |  86
 REF1        | 1099 |  84
 REF50       | 1627 |  76
 [...]
 REF47       |  217 |  23
 REF3        |  894 |  23
 REF4        | 1381 |  23
 REF63       |  148 |  22
 REF55       |   10 |  19
 REF46       |  607 |  19
 REF6        | 1724 |  16
 REF14       | 1534 |  16
 REF58       |  298 |  15
 REF23       |  387 |  14
(89 lignes)
-- le produit REF36 est celui avec le plus de problèmes
-- le produit REF23 est celui en ayant le moins.
-- il serait intéressant cependant de comparer avec les ventes de ces dits produits.
-- si un seul produit 23 a été vendu et 1000 pour 36, 23 serait donc celui qui a
-- le plus de problèmes avec 14 problèmes par produit contre 0.38 pour le 36.

-- un élément d'analyse intéresant serait aussi l'analyse par rapport au prix du
-- produit pour savoir si le prix influe sur la qualité. un autre point serait
-- aussi le cout de production pour savoir s'il est rentable ou non.



--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- L'analyse du nombre de défauts par catégorie est lui aussi intéressant

CREATE VIEW nbdefautscategorie AS
  SELECT d.label_cat, count(*) AS total
    FROM fbdl_exam.tickets t, fbdl_exam.defauts d
    WHERE t.fk_defaut = d.num_defaut
    GROUP BY d.label_cat
    ORDER BY d.label_cat ASC;
SELECT * FROM nbdefautscategorie;

label_cat  | total
-------------+-------
Body        |   205
Electricity |   223
Electronic  |   342
Engine      |   289
Equipment   |   385
Painting    |  3286
(6 lignes)
-- On peut vite voir qu'une majorité des défauts sont des défauts de painture
-- il faudrait pouvoir comparer le prix de chacun de ces problèmes

-- il serait aussi intéressant de voir cela suivant l'usine pour savoir si une
-- usine produit plus de défauts d'un certains type de catégorie, plutot qu'un
-- autre.





--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- nombre de défault par ticket, quand celui-ci en contient plus de deux

CREATE VIEW defaut3plus AS
  SELECT num_ticket, COUNT(*) as nb_defaut
    FROM fbdl_exam.tickets
    GROUP BY num_ticket
    HAVING  COUNT(*) > 2
    ORDER BY nb_defaut DESC;
SELECT * FROM defaut3plus;

-- affichage des 5 premières et dernière lignes
num_ticket  | nb_defaut
-------------+-----------
26735362204 |         4
06740250071 |         3
23382591127 |         3
02720550183 |         3
02606122502 |         3
[...]
13350810325 |         3
22076662674 |         3
15380820346 |         3
21352070872 |         3
06711360581 |         3
(46 lignes)

-- analyse de la catégorie de ces défauts
-- Requéte non finie



-- de nombreuses autres analyse sont possibles avec nos données actuelles. Des
-- analyses plus poussées peuvent être réalisées avec des données supplémentaire
-- notamment du service des ventes.
