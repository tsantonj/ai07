# Mini-projet évalué AI07 2019 (durée : 14h, points : 21)

Les données seront mises à disposition sur un ou des serveurs. 
Les données seront différentes pour chaque sujet.
Le projet ne sera **pas** corrigé au fur et à mesure, mais uniquement à la fin.

## Modèle 1 (1h, 5pts)
**dead line** : mardi 26 11h00
*Travail individuel. Tous documents autorisés, aucune communication autorisée avec des tiers. Réalisation avec les machines de la salle. Livraison papier ou numérique **par mail***

Réalisez un modèle dimensionnel en étoile à partir d'une analyse des besoins (fichier `contexte`) et d'une analyse des données. 

Le modèle intégrera :
- les faits et les dimensions
- la notation des hiérarchies (graphe des DF), 
- la typologie des attributs (analyse, segmentation, documentation)
- au moins un attribut d'agrégation de faits

On rendra également un exemple (pertinent) de requête décisionnelle et un extrait d'étude séparée des sources données.

## ETL 1 (1h, 5pts)
**dead line** : mardi 26 mars 12h00
*Travail individuel. Tous documents autorisés, aucune communication autorisée avec des tiers. Réalisation avec les machines de la salle. Livraison sur Git.*

Choisissez une question en rapport avec votre analyse et tentez d'y répondre en temps limité, en réalisant un ETL adapté.

## Modèle 2 (1h, 2pts)
**dead line** : jeudi 28 mars 8h00
*Travail à réaliser en autonomie. Livraison sur Git.*

Fournissez une seconde version de votre modèle dimensionnel.

## ETL 2 (5h, 2pts)
**dead line** : jeudi 28 mars 10h00
*Travail à réaliser en autonomie. Livraison sur Git.*

Fournissez une seconde version de votre ETL. 
Intégrer des rapports permettant de valider l'intégration de vos données.
Automatisez tout ou partie de vos traitements.

## Analyse libre (4h, 2pts)
**dead line** : mardi 2 avril 8h00
*Travail à réaliser en autonomie. Livraison sur Git.*

Fournissez un rapport d'analyse de vos données en travaillant sur les questions de votre choix en relation avec vos données. Vous formulerez chaque question en français et en SQL, montrerez les résultats bruts de façon lisible (données intégrales, extrait représentatif, graphique...) et formulerez une description des résultats en français.

## Analyse contrainte (2h, 5pts)
**dead line** : mardi 2 avril 10h00
*Travail individuel. Tous documents autorisés, aucune communication autorisée avec des tiers. Réalisation avec les machines de la salle. Livraison papier ou numérique **par mail***

En adoptant la même démarche que précédemment (question en français et SQL, présentation du résultat brut, description en français du résultat), vous répondrez à quatre questions imposées.