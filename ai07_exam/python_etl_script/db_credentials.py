# -*- coding: utf-8 -*-

# File Name     : db_credentials.py
# Description   : This file contains all sources databases informations
#
# Requirements  :
#
# Version       : 1.0
# Created on    : Sat Mar  9 12:31:06 2019
#
# @author       : Thibault Santonja
# git depot     : https://gitlab.utc.fr/tsantonj/ai07
#
# =============================================================================

# personal files
import variables

db_utc = {
    'host'      : variables.utc_host,
    'user'      : variables.user,
    'password'  : variables.password,
    'database'  : variables.datawarehouse_name
}


crzt_dep = {
    'source' : 'http://pic.crzt.fr/26/00006/dpt_fr',
    'file'      : 'departement.csv',
    'location'  : '~/Documents/ai07/ai07_exam/datas/'
}

crzt_prod = {
    'source' : 'http://pic.crzt.fr/26/00006/Product',
    'file'      : 'product.csv',
    'location'  : '~/Documents/ai07/ai07_exam/datas/'
}

crzt_def = {
    'source' : 'http://pic.crzt.fr/26/00006/Lawnmower00003',
    'file'      : 'Lawnmower00003.csv',
    'location'  : '~/Documents/ai07/ai07_exam/datas/'
}

crzt_factory = {
    'source' : 'http://pic.crzt.fr/26/00006/Factory',
    'file'      : 'factory.csv',
    'location'  : '~/Documents/ai07/ai07_exam/datas/'
}

crzt_deftype = {
    'source' : 'http://pic.crzt.fr/26/00006/DefaultType',
    'file'      : 'defaut.csv',
    'location'  : '~/Documents/ai07/ai07_exam/datas/'
}

crzt_categorie = {
    'source' : 'http://pic.crzt.fr/26/00006/Category',
    'file'      : 'categorie.csv',
    'location'  : '~/Documents/ai07/ai07_exam/datas/'
}
