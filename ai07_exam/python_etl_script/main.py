# -*- coding: utf-8 -*-

# File Name     : main.py
# Description   : <enter description>
#
# Requirements  : csv, pandas, wget,  psycopg2,
#                   sys
#
# Version       : 1.0
# Created on    : Sat Mar  9 12:27:33 2019
#
# @author       : Thibault Santonja
# git depot     : https://gitlab.utc.fr/tsantonj/ai07
#
# =============================================================================

import psycopg2
import sys
import pandas as pd
import wget
import csv
# personal files
import variables
import sql_queries
import db_credentials


# EXTRACT FROM HTML PAGE
def html_content_extract(source, file):
    # Open an url and extract data in a dataframe
    df = pd.read_csv(source, header=None, delimiter=';', encoding='utf-8')

    # Write the response in the .csv
    df.to_csv(file, sep=';', encoding='utf-8',
              header=None, index=False, quoting=csv.QUOTE_NONNUMERIC, quotechar='"')

    return {'column': list(df), 'file_size': df.size}

# GET DATASFILES
def recover_data(conn):
    departement = db_credentials.crzt_dep
    product = db_credentials.crzt_prod
    ticket = db_credentials.crzt_def
    usine = db_credentials.crzt_factory
    defType = db_credentials.crzt_deftype
    categorie = db_credentials.crzt_categorie

    # Extract Fantastic from html page to csv
    try:
        departement.update(html_content_extract(
            source = departement['source'],
            file = departement['location'] + departement['file']))
        product.update(html_content_extract(
            source = product['source'],
            file = product['location'] + product['file']))
        ticket.update(html_content_extract(
            source = ticket['source'],
            file = ticket['location'] + ticket['file']))
        usine.update(html_content_extract(
            source = usine['source'],
            file = usine['location'] + usine['file']))
        defType.update(html_content_extract(
            source = defType['source'],
            file = defType['location'] + defType['file']))
        categorie.update(html_content_extract(
            source = categorie['source'],
            file = categorie['location'] + categorie['file']))
    except Exception as e:
        print(e)
        sys.exit(e)

# CHECK DATAS
def check_data(f, cursor, table):
    cursor.execute("SELECT COUNT(*) from " + table + ";")
    dt_lines=cursor.fetchone()
    f.write("\n\n##%s\n" % (table))
    f.write("%s number of lines %s\r\n" % (table, str(dt_lines)))

    cursor.execute("SELECT * FROM " + table + " limit 10;")
    f.write("Table columns : \n")
    for column in cursor.description:
        f.write("- %s\n" % str(column[0]))

    f.write("\n10 first lines :\n```\n")
    for i in range(10):
        dt_lines=cursor.fetchone()
        f.write(str(dt_lines))
        f.write("\n")
    f.write("```\n")

def generate_log(conn):
    cursor = conn.cursor()
    f = open("log.md","w+")

    f.write("ETL data control\n===\n")

    f.write("\n\n#fbde control\n")
    for table in ['fbde_exam.produit', 'fbde_exam.departement', 'fbde_exam.defaut', 'fbde_exam.factory', 'fbde_exam.deftype', 'fbde_exam.cat']:
        check_data(f, cursor, table)

    f.write("\n\n#fbdt control\n")
    for table in ['fbdt_exam.produit', 'fbdt_exam.departement', 'fbdt_exam.defaut', 'fbdt_exam.factory', 'fbdt_exam.deftype', 'fbdt_exam.cat']:
        check_data(f, cursor, table)

    f.write("\n\n#fbdl control\n\n")
    f.write("every constraint were executed.\n")
    for table in ['fbdl_exam.dates', 'fbdl_exam.usines', 'fbdl_exam.produits', 'fbdl_exam.defauts', 'fbdl_exam.tickets']:
        check_data(f, cursor, table)

    f.close()
    cursor.close()

# CREATE E ZONE
def create_e_zone(conn):
    departement = db_credentials.crzt_dep
    product = db_credentials.crzt_prod
    ticket = db_credentials.crzt_def
    usine = db_credentials.crzt_factory
    defType = db_credentials.crzt_deftype
    categorie = db_credentials.crzt_categorie


    # Connection to database
    cursor = conn.cursor()

    cursor.execute(sql_queries.fbde_drop)
    conn.commit()

    # execute create schema and table
    cursor.execute(sql_queries.create_fbde)
    conn.commit()

    # copy datas
    #f = open('datas/' + departement['file'], 'r')
    #cursor.copy_from(f, 'fbde_exam.departement', sep=';')

    #f = open('datas/' + product['file'], 'r')
    #cursor.copy_from(f, 'fbde_exam.produit', sep=';')

    #f = open('datas/' + ticket['file'], 'r')
    #cursor.copy_from(f, 'fbde_exam.defaut', sep=';')

    #f = open('datas/' + usine['file'], 'r')
    #cursor.copy_from(f, 'fbde_exam.factory', sep=';')

    #f = open('datas/' + defType['file'], 'r')
    #cursor.copy_from(f, 'fbde_exam.deftype', sep=';')

    #f = open('datas/' + categorie['file'], 'r')
    #cursor.copy_from(f, 'fbde_exam.cat', sep=';')

    #fbde_copies = [sql_queries.fbde_copy_produit, sql_queries.fbde_copy_departement,
    #               sql_queries.fbde_copy_defaut, sql_queries.fbde_copy_factory,
    #               sql_queries.fbde_copy_deftype, sql_queries.fbde_copy_cat]

    #for copy in fbde_copies:
    #    try:
    #        cursor.execute(copy)
    #    except (Exception, psycopg2.DatabaseError) as error:
    #        print("create_e_zone:fbde_copy error")
    #        sys.exit(error)
    #cursor.execute(fbde_copy)
    #conn.commit()

    f = open('etl/import.sql', 'r')
    content = f.read()
    f.close()

    copies = content.split(';')

    for copy in copies:
        try:
            cursor.execute(copy)
        except:
            sys.exit()

    conn.commit()

    # Close cursor and connexion
    cursor.close()



# CREATE T ZONE
def create_t_zone(conn):
    # Connection to database
    cursor = conn.cursor()

    cursor.execute(sql_queries.fbdt_drop)
    conn.commit()

    # execute create schema and table
    cursor.execute(sql_queries.create_fbdt)
    conn.commit()

    # copy datas
    fbdt_insert = [sql_queries.fbdt_insert_produit, sql_queries.fbdt_insert_departement,
                   sql_queries.fbdt_insert_defaut, sql_queries.fbdt_insert_factory,
                   sql_queries.fbdt_insert_deftype, sql_queries.fbdt_insert_cat]

    for insert in fbdt_insert:
        try:
            cursor.execute(insert)
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print("create_t_zone:fbdt_insert error %s", insert)
            sys.exit(error)

    cursor.execute(sql_queries.fbdt_function_date)
    conn.commit()

    cursor.execute(sql_queries.fbdt_tmp_dt)
    conn.commit()

    fbdt_view = [sql_queries.fbdt_view_date, sql_queries.fbdt_view_ticket, sql_queries.fbdt_view_factory,
     sql_queries.fbdt_view_produit, sql_queries.fbdt_view_defaut]

    for view in fbdt_view:
        try:
            cursor.execute(view)
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print("create_t_zone:fbdt_view error")
            sys.exit(error)

    # Close cursor and connexion
    cursor.close()


# CREATE ZONE L
def create_l_zone(conn):
    # Connection to database
    cursor = conn.cursor()

    cursor.execute(sql_queries.fbdl_drop)
    conn.commit()

    # execute create schema and table
    cursor.execute(sql_queries.create_fbdl)
    conn.commit()

    # insert
    try:
        cursor.execute(sql_queries.fbdl_insert)
        conn.commit()
    except Exception as e:
        print('insert zone l error')
        raise e

    # insert
    try:
        cursor.execute(sql_queries.fbdl_constraint)
        conn.commit()
    except Exception as e:
        print('constraint zone l error')
        raise e

    # Close cursor and connexion
    cursor.close()


def create_etl(conn):
    print('get datas')
    recover_data(conn)

    print('create E zone')
    create_e_zone(conn)

    # control_data_e(conn)

    print('create T zone')
    create_t_zone(conn)

    # control_data_t(conn)

    print('create L zone')
    create_l_zone(conn)

    # control_data_l(conn)

    print('generation log file')
    generate_log(conn)




def sql_request(conn, query):
    # Connection to database
    cursor = conn.cursor()

    # execute a query
    try:
        cursor.execute(query)
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print('main:sql_request')
        sys.exit(error)

    # Close cursor and connexion
    cursor.close()



if __name__ == "__main__":

    # Get datas and create E Zone ---------------------------------------------
    db_utc = db_credentials.db_utc
    conn = None
    print('database connexion')
    print(variables.password + ' ' + variables.datawarehouse_name + ' ' + variables.utc_host)
    try:
        conn = psycopg2.connect(host=variables.utc_host,database=variables.datawarehouse_name,
                                user=variables.user, password=variables.password)
    except (Exception, psycopg2.DatabaseError) as error:
        print('main connexion error')
        sys.exit(error)

    create_etl(conn)

    if conn is not None:
        conn.close()

    print('everything done !')
