ETL exam 06
===


# Zone E

## Category
line number in file :

6 Category

line number in table :

 count
-------
     6
(1 ligne)


5 first lines of Category :

"A";"Painting"
"B";"Engine"
"C";"Body"
"D";"Electricity"
"E";"Electronic"

5 first lines of fbde_exam.cat :

 cat |    name     
-----+-------------
 A   | Painting
 B   | Engine
 C   | Body
 D   | Electricity
 E   | Electronic
(5 lignes)


## DefaultType
line number in file :

45 DefaultType

line number in table :

 count
-------
    45
(1 ligne)


5 first lines of DefaultType :

1;"C"
2;"F"
3;"F"
4;"A"
5;"B"

5 first lines of fbde_exam.deftype :

 type | cat
------+-----
 1    | C
 2    | F
 3    | F
 4    | A
 5    | B
(5 lignes)


## Factory
line number in file :

42 Factory

line number in table :

 count
-------
    42
(1 ligne)


5 first lines of Factory :

1;8;2;"44"
2;9;3;"49"
3;47;9;"27"
4;19;6;"91"
5;7;3;"37"

5 first lines of fbde_exam.factory :

 num | age | capa | loc
-----+-----+------+-----
 1   | 8   | 2    | 44
 2   | 9   | 3    | 49
 3   | 47  | 9    | 27
 4   | 19  | 6    | 91
 5   | 7   | 3    | 37
(5 lignes)


## Lawnmower00003
line number in file :

4730 Lawnmower00003

line number in table :

 count
-------
  4730
(1 ligne)


5 first lines of Lawnmower00003 :

"16290000001";2017-01-01;"REF30";17;36
"06680000002";2017-01-01;"REF69";7;20
"06680000002";2017-01-01;"REF69";7;42
"06680000003";2017-01-01;"REF69";7;31
"04030000003";2017-01-01;"REF4";5;34

5 first lines of fbde_exam.defaut :

     num     |     dt     | prod  | fact | type
-------------+------------+-------+------+------
 16290000001 | 2017-01-01 | REF30 | 17   | 36
 06680000002 | 2017-01-01 | REF69 | 7    | 20
 06680000002 | 2017-01-01 | REF69 | 7    | 42
 06680000003 | 2017-01-01 | REF69 | 7    | 31
 04030000003 | 2017-01-01 | REF4  | 5    | 34
(5 lignes)


## Product
line number in file :

89 Product

line number in table :

 count
-------
    89
(1 ligne)


5 first lines of Product :

"REF1";"#bcac12";1099;187;1821;12
"REF2";"#26aff4";369;179;1793;36
"REF3";"#beaff2";894;234;2233;30
"REF4";"#b72a56";1381;265;2451;40
"REF5";"#bfdb42";1302;192;2253;34

5 first lines of fbde_exam.produit :

 p_num |  color  | price | size | weight | d_age
-------+---------+-------+------+--------+-------
 REF1  | #bcac12 | 1099  | 187  | 1821   | 12
 REF2  | #26aff4 | 369   | 179  | 1793   | 36
 REF3  | #beaff2 | 894   | 234  | 2233   | 30
 REF4  | #b72a56 | 1381  | 265  | 2451   | 40
 REF5  | #bfdb42 | 1302  | 192  | 2253   | 34
(5 lignes)


## dpt_fr
line number in file :

95 dpt_fr

line number in table :

 count
-------
    95
(1 ligne)


5 first lines of Factory :

"01";"Ain";"529378"
"02";"Aisne";"552320"
"03";"Allier";"357110"
"04";"Alpes-de-Haute-Provence";"144809"
"05";"Hautes-Alpes";"126636"

5 first lines of fbde_exam.departement :

 num |           nom           |  pop   
-----+-------------------------+--------
 01  | Ain                     | 529378
 02  | Aisne                   | 552320
 03  | Allier                  | 357110
 04  | Alpes-de-Haute-Provence | 144809
 05  | Hautes-Alpes            | 126636
(5 lignes)



# Zone L


## fbdl_exam.dates
10 first lines

 date_full  | annee | trimestre | mois | semaine | jour_semaine
------------+-------+-----------+------+---------+--------------
 2017-10-17 |  2017 |         4 |   10 |      42 | Mardi
 2018-01-30 |  2018 |         1 |    1 |       5 | Mardi
 2018-08-14 |  2018 |         3 |    8 |      33 | Mardi
 2018-06-01 |  2018 |         2 |    6 |      22 | Vendredi
 2017-05-26 |  2017 |         2 |    5 |      21 | Vendredi
 2017-05-05 |  2017 |         2 |    5 |      18 | Vendredi
 2018-01-25 |  2018 |         1 |    1 |       4 | Jeudi
 2018-01-27 |  2018 |         1 |    1 |       4 | Samedi
 2018-10-26 |  2018 |         4 |   10 |      43 | Vendredi
 2017-01-26 |  2017 |         1 |    1 |       4 | Jeudi
(10 lignes)



## fbdl_exam.usines
10 first lines

 num_usine | age | capacite | localisation |         nom_dep         | pop_dep | cat_age | top_usine
-----------+-----+----------+--------------+-------------------------+---------+---------+-----------
        28 |  27 |        7 | 04           | Alpes-de-Haute-Provence |  144809 |       3 |         0
        12 |  23 |        4 | 05           | Hautes-Alpes            |  126636 |       3 |         0
        41 |  42 |        8 | 08           | Ardennes                |  299166 |       5 |         1
        23 |  35 |        8 | 09           | Ariège                  |  142834 |       4 |         1
        15 |  14 |        6 | 11           | Aude                    |  319611 |       2 |         0
        24 |   9 |        5 | 17           | Charente-Maritime       |  579187 |       1 |         0
        38 |  40 |        4 | 18           | Cher                    |  325131 |       5 |         0
        42 |  24 |        4 | 18           | Cher                    |  325131 |       3 |         0
        29 |  40 |        5 | 20           | Corse                   |  267249 |       5 |         0
         8 |  12 |        4 | 26           | Drôme                   |  450732 |       2 |         0
(10 lignes)



## fbdl_exam.produits
10 first lines

 num_produit | couleur | prix | taille | poids | design_age | cat_age
-------------+---------+------+--------+-------+------------+---------
 REF1        | #bcac12 | 1099 |    187 |  1821 |         12 |       3
 REF2        | #26aff4 |  369 |    179 |  1793 |         36 |       8
 REF3        | #beaff2 |  894 |    234 |  2233 |         30 |       7
 REF4        | #b72a56 | 1381 |    265 |  2451 |         40 |       9
 REF5        | #bfdb42 | 1302 |    192 |  2253 |         34 |       7
 REF6        | #612fb6 | 1724 |    203 |  2396 |         34 |       7
 REF7        | #e92b32 | 1360 |     42 |   364 |         35 |       8
 REF8        | #d33bb4 | 1352 |    175 |  1404 |         36 |       8
 REF9        | #f128f4 |  151 |    113 |  1322 |         18 |       4
 REF10       | #033db6 |  971 |    131 |  1102 |         31 |       7
(10 lignes)



## fbdl_exam.defauts
10 first lines

 num_defaut | categorie | label_cat
------------+-----------+-----------
         29 | A         | Painting
         27 | A         | Painting
          4 | A         | Painting
          7 | A         | Painting
          9 | A         | Painting
         42 | A         | Painting
         39 | A         | Painting
         35 | A         | Painting
         34 | A         | Painting
         31 | A         | Painting
(10 lignes)



## fbdl_exam.tickets
10 first lines

 num_ticket  | fk_usine |  fk_date   | fk_defaut | fk_produit | quantity
-------------+----------+------------+-----------+------------+----------
 16290000001 |       17 | 2017-01-01 |        36 | REF30      |         
 06680000002 |        7 | 2017-01-01 |        20 | REF69      |         
 06680000002 |        7 | 2017-01-01 |        42 | REF69      |         
 06680000003 |        7 | 2017-01-01 |        31 | REF69      |         
 04030000003 |        5 | 2017-01-01 |        34 | REF4       |         
 04030000004 |        5 | 2017-01-01 |        40 | REF4       |         
 23810000005 |       24 | 2017-01-01 |        31 | REF82      |         
 13860000006 |       14 | 2017-01-01 |        42 | REF87      |         
 04720010006 |        5 | 2017-01-02 |        35 | REF73      |         
 09830020007 |       10 | 2017-01-03 |        42 | REF84      |         
(10 lignes)
