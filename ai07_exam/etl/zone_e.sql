DROP SCHEMA fbde_exam CASCADE;

CREATE SCHEMA fbde_exam
  CREATE TABLE produit (
    p_num text,
    color text,
    price text,
    size text,
    weight text,
    d_age text
  )

  CREATE TABLE departement (
    num text,
    nom text,
    pop text
  )

  CREATE TABLE defaut (
    num text,
    dt text,
    prod text,
    fact text,
    type text
  )

  CREATE TABLE factory (
    num text,
    age text,
    capa text,
    loc Text
  )

    CREATE TABLE deftype (
      type text,
      cat text
    )

    CREATE TABLE cat (
      cat text,
      name text
    )

  GRANT ALL PRIVILEGES ON produit, departement, defaut, factory, deftype, cat TO bdd1p199;

-- import / copy des CSV dans les tables précédemment créées
\copy fbde_exam.produit FROM '~/Documents/Product' DELIMITER ';' CSV;
\copy fbde_exam.departement FROM '~/Documents/dpt_fr' DELIMITER ';' CSV;
\copy fbde_exam.defaut FROM '~/Documents/Lawnmower00003' DELIMITER ';' CSV;
\copy fbde_exam.factory FROM '~/Documents/Factory' DELIMITER ';' CSV;
\copy fbde_exam.deftype FROM '~/Documents/DefaultType' DELIMITER ';' CSV;
\copy fbde_exam.cat FROM '~/Documents/Category' DELIMITER ';' CSV;


-- RESULTS
--
--dbbdd1p002=> \copy fbde_exam.produit FROM '~/Documents/exam/product.csv' DELIMITER ';' CSV;
--COPY 89
--dbbdd1p002=> \copy fbde_exam.departement FROM '~/Documents/exam/departement.csv' DELIMITER ';' CSV;
--COPY 95
--dbbdd1p002=> \copy fbde_exam.defaut FROM '~/Documents/exam/Lawnmower00003.csv' DELIMITER ';' CSV;
--COPY 4730
--dbbdd1p002=> \copy fbde_exam.factory FROM '~/Documents/exam/factory.csv' DELIMITER ';' CSV;
--COPY 42
--dbbdd1p002=> \copy fbde_exam.deftype FROM '~/Documents/exam/defaut.csv' DELIMITER ';' CSV;
--COPY 45
--dbbdd1p002=> \copy fbde_exam.cat FROM '~/Documents/exam/categorie.csv' DELIMITER ';' CSV;
--COPY 6
