-- nombres de défauts par produits
DROP VIEW nbdefautsproduit;
CREATE VIEW nbdefautsproduit AS
  SELECT p.num_produit, p.prix, COUNT(*) AS Pb
    FROM fbdl_exam.produits p, fbdl_exam.tickets t
    WHERE t.fk_produit = p.num_produit
    GROUP BY p.num_produit
    ORDER BY Pb DESC;

SELECT * FROM nbdefautsproduit;

num_produit | prix | pb
-------------+------+-----
 REF36       |   29 | 383
 REF52       |  513 | 222
 REF39       | 1110 | 220
 REF19       | 1621 | 112
 REF24       |  499 | 108
 REF73       |  175 |  94
 REF43       |  397 |  86
 REF30       | 1055 |  86
 REF1        | 1099 |  84
 REF50       | 1627 |  76
 REF69       |  823 |  75
 REF82       |  645 |  74
 REF34       |  487 |  73
 REF84       | 1480 |  72
 REF56       |   91 |  65
 REF62       |  494 |  63
 REF29       |  964 |  63
 REF65       | 1239 |  61
 REF40       |  702 |  60
 REF20       |  835 |  60
 REF89       |  900 |  60
 [...]
 REF8        | 1352 |  26
 REF83       | 1971 |  25
 REF27       | 1037 |  24
 REF49       |  181 |  24
 REF38       |  140 |  23
 REF47       |  217 |  23
 REF3        |  894 |  23
 REF4        | 1381 |  23
 REF63       |  148 |  22
 REF55       |   10 |  19
 REF46       |  607 |  19
 REF6        | 1724 |  16
 REF14       | 1534 |  16
 REF58       |  298 |  15
 REF23       |  387 |  14
(89 lignes)
-- le produit REF36 est celui avec le plus de problèmes
-- le produit REF23 est celui en ayant le moins.
-- il serait intéressant cependant de comparer avec les ventes de ces dits produits.
-- si un seul produit 23 a été vendu et 1000 pour 36, 23 serait donc celui qui a le plus de problèmes




-- nombres de défauts par usine
DROP VIEW nbdefauts;
CREATE VIEW nbdefauts AS
  SELECT COUNT(*) as nbDefaut, usines.num_usine
    FROM fbdl_exam.tickets
    INNER JOIN fbdl_exam.usines ON tickets.fk_usine = usines.num_usine
    GROUP BY usines.num_usine
    ORDER BY nbDefaut DESC;
SELECT * FROM nbdefauts;

nbdefaut | num_usine
----------+-----------
     276 |        32
     270 |         3
     218 |        23
     216 |        27
     212 |        41
     210 |         7
     205 |         6
     204 |        11
     199 |        19
     169 |         9
     162 |        13
     162 |        16
     158 |        33
     140 |        28
     132 |        14
     126 |        21
     124 |         4
     119 |        17
     118 |        39
     114 |        29
     110 |        15
104 |        36
 96 |        25
 81 |        22
 79 |        24
 76 |        34
 76 |        18
 72 |        38
 67 |        10
 67 |         8
 61 |        42
 52 |        12
 46 |        20
 40 |        40
 35 |        31
 30 |        37
 28 |         5
 24 |        30
 16 |        35
 15 |         2
 14 |         1
  7 |        26
(42 lignes)
-- on peut voir que l'usine 32 est celle qui a eu le plus de problèmes (276).
-- L'usine 3 est très proche de ce résultat avec 270 problèmes
-- L'usine 26 est celle avec le moins de problèmes
-- il faudrait pouvoir analyser le nombre de produit produits par usine



-- nb defaut / ticket
DROP VIEW nbdefautscategorie;
CREATE VIEW nbdefautscategorie AS
  SELECT d.label_cat, count(*) AS total
    FROM fbdl_exam.tickets t, fbdl_exam.defauts d
    WHERE t.fk_defaut = d.num_defaut
    GROUP BY d.label_cat
    ORDER BY d.label_cat ASC;
SELECT * FROM nbdefautscategorie;

label_cat  | total
-------------+-------
Body        |   205
Electricity |   223
Electronic  |   342
Engine      |   289
Equipment   |   385
Painting    |  3286
(6 lignes)
-- On peut vite voir qu'une majorité des défauts sont des défauts de painture
-- il faudrait pouvoir comparer le prix de chacun de ces problèmes




-- analyse par jour de la semaine
DROP VIEW defautjds;
CREATE VIEW defautjds AS
  SELECT d.jour_semaine AS JDS, COUNT(*) AS Ventes
    FROM fbdl_exam.dates d, fbdl_exam.tickets t
    WHERE d.date_full = t.fk_date
    GROUP BY d.jour_semaine
    ORDER BY d.jour_semaine='Lundi', d.jour_semaine='Mardi', d.jour_semaine='Mercredi', d.jour_semaine='Jeudi', d.jour_semaine='Vendredi', d.jour_semaine='Samedi', d.jour_semaine='Dimanche';
SELECT * FROM defautjds;

jds    | ventes
----------+--------
Dimanche |    555
Samedi   |    764
Vendredi |    859
Jeudi    |    675
Mercredi |    690
Mardi    |    660
Lundi    |    527
(7 lignes)

-- on voit plus de problèmes réglés le vendredi
