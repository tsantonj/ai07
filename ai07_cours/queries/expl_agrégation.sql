-- Effectuez une requête de type GROUP BY pour étudier les ventes en fonction du jour de la semaine.
CREATE OR REPLACE VIEW fbdl.ventePjds AS
  SELECT j_semaine, count(*)
    FROM fbdt.ventes, fbdt.dates
    WHERE fbdt.ventes.fk_date = fbdt.dates.pk_date
    GROUP BY j_semaine;

SELECT * FROM fbdl.ventePjds;
    j_semaine | count
   -----------+--------
    Mardi     |  70370
    Vendredi  |  70142
    Mercredi  |  70108
    Samedi    | 175260
    Jeudi     |  72516


-- Écrivez une requête avec sous-requête dans la clause FROM pour trouver la moyenne des ventes des jours hors samedi.


-- Transformez la première requête permettant de calculer les ventes en fonction du jour de la semaine en une vue.
-- Ajouter deux autres vues :
--     la première calculera la moyenne des ventes hors samedi ;
--     la seconde le ratio de ventes le samedi par rapport à la moyenne.
