Analyse des données, cas Fantastic
===

# Analyse des données
La qualité est doonnée en 4 niveaux:
- 0 : données inexploitables
- 1 : données peu exploitables (traitements incertains)
- 2 : données exploitables après traitements
- 3 : données exploitables sans traitement

L'utilité est donnée en 4 niveaux:
- 0 : données sans intérêt
- 1 : données utiles pour la documentation uniquement
- 2 : données utiles a priori
- 3 : données utiles avec certitude


## Catalogue des livres

Nom: catalogue
Origin: Oracle
Lignes: 1443

| Nom | Type | Description | Qualité | Utilité | Commentaire |
| :-----------: | :-------------: | :------------: | :-----------: | :-------------: | :------------: |
| Référence | NUMBER(38) |         | 3 | 3 | Souvent composé de 4 chiffres, certaines références n'en ont que 3 voire 1 |
| ISBN | varchar(3)varchar(13) | International Standard Book Number | 3 | 3 | doivent être composés de 13 chiffres, certain n'en ont que 3 |
| Titre | VARCHAR2(255) |  | 2 | 1 |  |
| Auteur | VARCHAR2(255) |  | 2 | 3 | Necessite un traitement des données, parfois plusieurs auteurs sont données |
| Langage | VARCHAR2(3) |  | 2 | 1 | parfois nul |
| Date de publication | VARCHAR2(25) |  | 2 | 3 | A reformater en format date |
| Editeur | VARCHAR2(255) |  | 0 | 0 |  |
| Genre | VARCHAR2(255) |  | 3 | 1 |  |
| Tag | VARCHAR2(255) |  | 1 | 1 |  |


## Base de donnée des départements

Nom: departement_insee_2003
Origin: [data.gouv](https://www.data.gouv.fr/fr/)
Lignes: 95

| Nom | Type | Description | Qualité | Utilité | Commentaire |
| :-----------: | :-------------: | :------------: | :-----------: | :-------------: | :------------: |
| Numéro | Varchar(2) | numéro du département | 3 | 3 |  |
| Nom | Varchar |  | 2 | 2 | certains caractères spéciaux ont des soucis d'affichage |
| Population | Varchar |  | 3 | 3 | quantité numérique |


## Magasin

Nom: marketing
Origin: [pic.crzt.fr/ai07](http://pic.crzt.fr/ai07/fantastic/)
Lignes: 152

| Nom | Type | Description | Qualité | Utilité | Commentaire |
| :-----------: | :-------------: | :------------: | :-----------: | :-------------: | :------------: |
| Magasin | Varchar | 'M' suivit d'un numéro | 3 | 3 |  |
| Département | Entier |  | 3 | 3 |  |
| Rayonnage | Char |  | 3 | 3 | 3 type possible, A : organisation par auteur, E : par éditeur, Y: par année |
| Rayon bestseller | Booléen |  | 3 | 3 |  |
| Rayon récent | Booléen |  | 3 | 2 |  |



## Ventes

Nom: Fantastic
Origin: [pic.crzt.fr/ai07](http://pic.crzt.fr/ai07/fantastic/)
Lignes: 512018

| Nom | Type | Description | Qualité | Utilité | Commentaire |
| :-----------: | :-------------: | :------------: | :-----------: | :-------------: | :------------: |
| Numéro ticket | entier |  | 2 | 3 | certains enregistrement n'ont pas d'enregistrement => enregistrement inexploitable à supprimer |
| Date | JJ-MM-AAAA |  | 2 | 3 | Certaines dates nulles, certaines dates avec le jour ou/et le mois ou/et l'années manquants et certains formats mauvais (formules mathématiques) => enregistrement inexploitable à supprimer |
| produit | Varchar(13) | ISBN | 2 | 3 | certains sont nulls => enregistrement inexploitable à supprimer |
| Magasin | Varchar |  | 2 | 3 | Certains sont nuls ou ne possèdent pas de chiffres après le M => enregistrement inexploitable à supprimer |


# Modèle relationnel

![](fantastic.png "Modèle relationnel sous-jacent aux données présentes.")