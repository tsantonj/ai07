Analyse des besoins, cas Fantastic
===

# Requête décisionnelle

:::info
Une **Requête décisionnelle** exprime toujours une quantification de **fait** par rapport à des **dimensions** `Quelle a été la quantité de ... (fait) en fonction de ... (dimension)`

**Un _fait_ est une grandeur que l'on souhaite mesurer (prix, quantité, ...)**
**Une _dimension_ est un axe d'analyse (date, lieu, personne, ...)**
:::

## Service marketing

> La direction marketing est en charge de l’implantation des magasins dans les départements et de l'organisation des rayonnages (type de rangement et présence de rayons spécifiques pour les best-sellers). Elle cherche à savoir si l'organisation du rayonnage des magasins a une influence sur les volumes ventes, et si cela varie en fonction des jours de la semaine ou de certaines périodes de l'année. Elle voudrait également savoir si certains magasins ou départements sont plus dynamiques que d'autres.

_Quelle a été la quantité de livres vendus en fonction des rayons et du mois de l'année_
```
Quantité de livres vendus
/ Organisation rayons
/ Mois
```

_Quelle a été la quantité de livres vendus en fonction des rayons et des jours de la semaine_
```
Quantité de livres vendus
/ Organisation rayons
/ Jour de la semaine
```

_Quelle a été la quantité de livres vendus en fonction des rayons, des jours de la semaine et du mois de l'année_
```
Quantité de livres vendus
/ Organisation rayons
/ Jour de la semaine
/ Mois
```

_Quelle a été la quantité de livres vendus en fonction de la présence d'un rayon best seller, des jours de la semaine et du mois de l'année_
```
Quantité de livres vendus
/ Rayons Best Seller
/ Jour de la semaine
/ Mois
```

_Quelle a été la quantité de livres vendus en fonction de la présence d'un rayon best seller, de l'organisation des rayons, des jours de la semaine et du mois de l'année_
```
Quantité de livres vendus
/ Rayons Best Seller
/ Organisation rayons
/ Jour de la semaine
/ Mois
```

_Quelle a été la quantité de livres vendus en fonction des régions et de la population de la région_
```
Quantité de livres vendus
/ Région
/ Population de la région
```

Sachant que deux régions différentes n'ont pas la même population, il peut être trompeur et peu significatif de regarder exclusivement suivant la région. Par exemple, constater que l'on vend 10 fois moins dans le Limousin comparé à l'Île de France est trompeur sachant que la population y est 17 fois plus faible.

_Quelle a été la quantité de livres vendus en fonction de la quantité de magasin par département, des département et de la population du département_
```
Quantité de livres vendus
/ Région
/ Population de la région
/ Magasin
```

De même, il est intéressant de regarder la quantité de magasins présents dans le département. Il est assez évident que si l'on n'a qu'un seul magasin dans le département, les ventes dans le département seront certainement plus faibles que dans un département avec 10 magasins.


**Conclusion**
```
Quantité de livres vendus
/ Région
/ Population de la région
/ Magasin
/ Organisation rayons
/ Rayons Best Seller
/ Jour de la semaine
/ Mois
```



## Service éditorial

> La direction éditoriale se demande si certains livres se vendent mieux à certaines dates et/ou dans certains magasins ou départements. Elle aimerait également savoir si certains auteurs ou éditeurs se vendent mieux, et s'il existe un lien entre l'ancienneté des livres et les ventes. Elle se demande aussi si certaines périodes sont plus propices que d'autres à l'écoulement des livres les plus anciens.

_Quelle a été la quantité de livres vendus en fonction des département et de la population du département_
```
Quantité de livres vendus
/ Vente
/ Département
/ Population du département
```

_Quelle a été la quantité de livres vendus en fonction du jour de l'année_
```
Quantité de livres vendus
/ Vente
/ Jour
```

_Quelle a été la quantité de livres vendus en fonction du jour de l'année, de la densité de population du département_
```
Quantité de livres vendus
/ Vente
/ Jour
/ Département
/ Population du département
```

_Quelle a été la quantité de livres vendus en fonction de l'auteur et en fonction du nombre de livres de cet auteur_
```
Quantité de livres vendus
/ Vente
/ Auteur
```

Il est ici important de souligner qu'un auteur donné aura peut être plus de livres vendus mais il en a peut être ecrit plus. Par exemple, on trouve généralement beaucoup de livres de Victor Hugo et moins de De la Fontaine. Ainsi, pour un livre donné, ils vendent possiblement autant, mais au total, Victor Hugo vendra plus.

_Quelle a été la quantité de livres vendus en fonction l'année de publication et en fonction du nombre de livres publiés l'année donnée_
```
Quantité de livres vendus
/ Vente
/ Année
```

_Quelle a été la quantité de livres vendus en fonction l'année de publication,  en fonction du nombre de livres publiés l'année donnée et suivant le mois de l'année de la vente_
```
Quantité de livres vendus
/ Vente
/ Année
```


**Conclusion**
```
Quantité de livres vendus
/ Vente
/ Auteur
/ Année
/ Jour
/ Département
/ Population du département
```