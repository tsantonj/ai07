DROP TABLE fbdt.ventes, fbdt.magasins, fbdt.dates, fbdt.livres;
DROP SCHEMA fbdt;

CREATE SCHEMA fbdt
  CREATE TABLE ventes (
    numero      text,
    fk_date     date,
    fk_magasin  varchar(4),
    fk_isbn     varchar(13),
    quantite    integer,
    CONSTRAINT checkventesnum CHECK(numero IS NOT NULL)
    --CONSTRAINT checkventesdt CHECK(fk_date IS NOT NULL),
    --CONSTRAINT checkventesmag CHECK(fk_magasin IS NOT NULL),
    --CONSTRAINT checkventesisbn CHECK(fk_isbn IS NOT NULL)
  )

  CREATE TABLE magasins (
    pk_magasin  varchar(4),
    rayon       char(1) CHECK (rayon IN ('A', 'E', 'Y')),
    r_full      varchar(7) CHECK (r_full IN ('Author', 'Editor', 'Year')),
    r_bs        boolean,
    r_new       boolean,
    dep_num     char(2),
    dep_nom     varchar(255),
    dep_pop     integer,
    reg_nom     varchar(255),
    CONSTRAINT checkmag CHECK(pk_magasin IS NOT NULL)
  )

  CREATE TABLE dates ( --TODO Extraire les données de la pk
    pk_date     date,
    annee       numeric(4),
    mois        numeric(2),
    jour        numeric(2),
    trimestre   numeric(1),
    semaine     numeric(2),
    j_semaine   varchar(10),
    CONSTRAINT checkdate CHECK(pk_date IS NOT NULL)
  )

  CREATE TABLE livres (
    pk_isbn     varchar(13),
    reference   varchar(7),
    titre       varchar(255),
    auteur      varchar(255),
    editeur     varchar(255),
    genre       varchar(255),
    langage     char(3),
    anne_pub    numeric(4),
    age         numeric(4),
    CONSTRAINT checkisbn CHECK(pk_isbn IS NOT NULL)
  )

  GRANT ALL PRIVILEGES ON ventes, magasins, dates, livres TO bdd1p199;

---------------------------------------

CREATE OR REPLACE FUNCTION checkDate(dt IN text) RETURNS DATE AS
$$
BEGIN
  IF dt SIMILAR TO '[\d]{4}-[\d]{2}-[\d]{2}' THEN
      return TO_DATE(dt, 'YYYY-MM-DD');
  ELSIF dt SIMILAR TO '[\d]{4}-[\d]{2}-[\d]{2}T[\d]{2}:[\d]{2}:[\d]{2}\+[\d]{2}:[\d]{2}' THEN
      return TO_DATE(dt, 'YYYY-MM-DD');
  ELSE
      return TO_DATE(dt, '0001-01-01');
  END IF;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION checkMagasin(magasin VARCHAR(4)) RETURNS VARCHAR(4) AS
$$
BEGIN
  IF magasin SIMILAR TO 'M(\d){1,3}' THEN
      return magasin;
  ELSE
      return concat('M', magasin);
  END IF;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION checkisbn(isbn text) RETURNS CHAR(13) AS
$$
BEGIN
  IF isbn='?' OR isbn='' OR isbn SIMILAR TO ' *' THEN
    RETURN NULL;
  ELSIF length(isbn) > 13 THEN
    RETURN NULL;
  ELSE
    IF length(isbn)=13 OR length(isbn)>13 AND length(isbn)<0 THEN
      RETURN isbn;
    ELSE
      RETURN NULL;
    END IF;
  END IF;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION checkfkmagasin(num VARCHAR(4)) RETURNS VARCHAR(4) AS
$$
DECLARE
  tmp INTEGER;
BEGIN
  IF num SIMILAR TO 'M(\d){1,3}' THEN
    SELECT COUNT(*) INTO tmp FROM fbdt.magasins WHERE fbdt.magasins.pk_magasin = num;
    IF tmp <> 0 THEN
        RETURN num;
      ELSE
        RETURN NULL;
      END IF;
    ELSE
      RETURN NULL;
  END IF;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION checkfkisbn(isbn VARCHAR(13)) RETURNS CHAR(13) AS
$$
DECLARE
  tmp INTEGER;
BEGIN
    IF char_length(isbn) = 13 THEN
        RETURN CAST(isbn AS CHAR(13));
    ELSIF char_length(isbn) = 3 OR char_length(isbn) = 10 OR char_length(isbn) = 9 THEN
        SELECT COUNT(*) INTO tmp FROM fbdt.livres WHERE fbdt.livres.pk_isbn = CAST(isbn AS CHAR(13));
        IF tmp <> 0 THEN
          RETURN CAST(isbn AS CHAR(13));
        ELSE
          RETURN NULL;
        END IF;
    ELSE
      RETURN NULL;
  END IF;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION getage(givendate text) RETURNS numeric(4) AS
$$
BEGIN
  RETURN date_part('year', NOW())-date_part('year', checkDate(givendate));
END;
$$ LANGUAGE plpgsql;


-- données magasins ----------------------------------------------------------

INSERT INTO fbdt.magasins(pk_magasin, r_full, rayon, r_bs, r_new, dep_num)
  SELECT checkMagasin(magasin), Rayonnage_full, rayonnage,
    CASE WHEN r_bestseller='0' THEN False WHEN r_bestseller='1' THEN True END,
    CASE WHEN r_recent='0' THEN False WHEN r_recent='1' THEN True END,
    departement
  FROM fbde.magasin
  WHERE rayonnage IS NOT NULL
    AND magasin IS NOT NULL;

UPDATE fbdt.magasins SET
  dep_nom = fbde.departement.nom,
  dep_pop = CAST(fbde.departement.population AS numeric(9))
  FROM fbde.departement
  WHERE fbdt.magasins.dep_num = CAST(CAST(fbde.departement.numero AS integer) AS CHAR(2));

INSERT INTO fbdt.magasins(pk_magasin) VALUES ('M');
-- données dates -------------------------------------------------------------------

-- ALTER TABLE fbdt.dates
--   DROP COLUMN pk_date,
--   ADD pk_date varchar(10);
INSERT INTO fbdt.dates(pk_date)
  SELECT DISTINCT(checkDate(dt))
  FROM fbde.ticket
  WHERE checkDate(dt) IS NOT NULL;

UPDATE fbdt.dates SET
  annee = date_part('year', pk_date),
  mois = date_part('month', pk_date),
  jour = date_part('day', pk_date),
  trimestre = date_part('quarter', pk_date),
  semaine = date_part('week', pk_date),
  j_semaine = CASE
    WHEN date_part('dow', pk_date) = 1 THEN 'Lundi'
    WHEN date_part('dow', pk_date) = 2 THEN 'Mardi'
    WHEN date_part('dow', pk_date) = 3 THEN 'Mercredi'
    WHEN date_part('dow', pk_date) = 4 THEN 'Jeudi'
    WHEN date_part('dow', pk_date) = 5 THEN 'Vendredi'
    WHEN date_part('dow', pk_date) = 6 THEN 'Samedi'
    WHEN date_part('dow', pk_date) = 0 THEN 'Dimanche'
  END;

--INSERT INTO fbdt.dates(pk_date) VALUES (checkDate('0001-01-01'));


-- données livres ------------------------------------------------------------
INSERT INTO fbdt.livres(pk_isbn, reference, titre, auteur, editeur, genre, langage, anne_pub, age)
  SELECT isbn, ref, title, authors, publisher, genre, language, date_part('year', checkDate(pubdate)), getage(pubdate)
  FROM fbde.catalogue;

--INSERT INTO fbdt.livres(pk_isbn, reference, titre, auteur, editeur, genre, langage, anne_pub, age)
--  VALUES ('0000000000000', 'null', 'null', 'null', 'null', 'null', 'nil', date_part('year', checkDate('0001-01-01')), date_part('year', NOW())-date_part('year', checkDate('0001-01-01')));

INSERT INTO fbdt.livres(pk_isbn)
  SELECT fk_isbn
  FROM fbdt.ventes
  WHERE fbdt.ventes.fk_isbn NOT IN (SELECT DISTINCT pk_isbn FROM fbdt.livres);


-- Data du fait (ventes) ---------------------------------------------------
INSERT INTO fbdt.ventes(numero, fk_date, fk_isbn, fk_magasin, quantite)
  SELECT DISTINCT numero, checkDate(dt), checkisbn(produit), checkMagasin(magasin), (SELECT count(numero) FROM fbdt.ventes GROUP BY fk_date, fk_isbn, fk_magasin)
  FROM fbde.ticket;
 -- WHERE checkDate(dt) IS NOT NULL
   -- AND checkisbn(produit) IS NOT NULL
    --AND checkMagasin(magasin) IS NOT NULL;


--  SELECT count(numero) FROM fbdt.ventes GROUP BY fk_date, fk_isbn, fk_magasin;
--  SELECT COUNT(id) FROM (SELECT DISTINCT numero, fk_date, fk_isbn, fk_magasin  FROM fbdt.ventes) as id;
