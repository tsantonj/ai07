-- rajouter des contraintes strictes
DROP TABLE fbdl.ventes, fbdl.magasins, fbdl.dates, fbdl.livres;
DROP SCHEMA fbdl;
CREATE SCHEMA fbdl
  CREATE TABLE magasins (
    pk_magasin  varchar(4) PRIMARY KEY,
    rayon       char CHECK (rayon IN ('A', 'E', 'Y')),
    r_full      varchar(7) CHECK (r_full IN ('Author', 'Editor', 'Year')),
    r_bs        boolean,
    r_new       boolean,
    dep_num     char(2),
    dep_nom     varchar(255),
    dep_pop     integer,
    reg_nom     varchar(255)
  )

  CREATE TABLE dates (
    pk_date     date PRIMARY KEY,
    annee       numeric(4),
    mois        numeric(2),
    jour        numeric(2),
    trimestre   numeric(1),
    semaine     numeric(2),
    j_semaine   varchar(10)
  )

  CREATE TABLE livres (
    pk_isbn     char(13) PRIMARY KEY,
    reference   varchar(20),
    titre       varchar(255),
    auteur      varchar(255),
    editeur     varchar(255),
    genre       varchar(255),
    langage     char(3),
    anne_pub    numeric(4) CHECK (anne_pub BETWEEN 0001 AND date_part('year', NOW())),
    age         integer
  )

  CREATE TABLE ventes (
    numero      text,
    fk_date     date references dates(pk_date), -- NOT NULL,
    fk_magasin  varchar(4) references magasins(pk_magasin), -- NOT NULL,
    fk_isbn     char(13) references livres(pk_isbn), -- NOT NULL,
    quantite    integer
    --PRIMARY KEY(numero, fk_date, fk_magasin, fk_isbn)
  )

  GRANT ALL PRIVILEGES ON livres, dates, magasins, ventes TO bdd1p199;


-- données magasins ----------------------------------------------------------
INSERT INTO fbdl.magasins(pk_magasin, rayon, r_full,r_bs, r_new,dep_num, dep_nom, dep_pop, reg_nom)
  SELECT pk_magasin, rayon, r_full,r_bs, r_new,dep_num, dep_nom, dep_pop, reg_nom
  FROM fbdt.magasins;

-- données dates -------------------------------------------------------------------
INSERT INTO fbdl.dates(pk_date, annee, mois, jour, trimestre, j_semaine)
  SELECT pk_date, annee, mois, jour, trimestre, j_semaine
  FROM fbdt.dates;
-- données dates -------------------------------------------------------------------
INSERT INTO fbdl.livres(pk_isbn, reference, titre, auteur, editeur, genre, langage, anne_pub, age)
  SELECT pk_isbn, reference, titre, auteur, editeur, genre, langage, anne_pub, age
  FROM fbdt.livres;

INSERT INTO fbdl.ventes
  SELECT *
  FROM fbdt.ventes;
