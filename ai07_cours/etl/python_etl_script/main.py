# -*- coding: utf-8 -*-

# File Name     : main.py
# Description   : <enter description>
#
# Requirements  : cx_Oracle, csv, pandas, wget, pyexcel, pyexcel-ods, psycopg2,
#                   sys
#
# Version       : 1.0
# Created on    : Sat Mar  9 12:27:33 2019
#
# @author       : Thibault Santonja
# git depot     : https://gitlab.utc.fr/tsantonj/ai07
#
# =============================================================================

import psycopg2
import sys
import pandas as pd
# personal files
import variables
import etl_e
import etl_t
import etl_l
import sql_queries
import db_credentials


def recover_data(conn):
    fantastic_conf = db_credentials.ods_db_fantastic_config
    catalogue_conf = db_credentials.oracle_db_catalogue_config
    marketing_conf = db_credentials.ods_db_marketing_config
    departement_conf = db_credentials.csv_db_departement_config

    # departement
    file = departement_conf['location'] + departement_conf['file']
    df = pd.read_csv(file)
    departement_conf.update({'file_size': df.size})

    # Extract catalogue from Oracle Database to csv
    try:
        # TODO plantage oracle connexion = cx_Oracle.connect(user, password, dsn_tns)
        # cx_Oracle.DatabaseError: DPI-1047: Cannot locate a 64-bit Oracle Client library: "libclntsh.so: cannot open shared object file: No such file or directory". See https://oracle.github.io/odpi/doc/installation.html#linux for help
        # Exit 1
        catalogue_conf.update(etl_e.extract_from_oracle(
            host = catalogue_conf['host'],
            port = catalogue_conf['port'],
            sid = catalogue_conf['sid'],
            user = catalogue_conf['user'],
            password = catalogue_conf['password'],
            database = catalogue_conf['database'],
            table = 'Catalogue',
            file = catalogue_conf['location'] + catalogue_conf['file']))

    except Exception as e:
        print(e)
        raise

    # Extract Fantastic from html page to csv
    try:
        fantastic_conf.update(etl_e.html_content_extract(
            source = fantastic_conf['source'],
            file = fantastic_conf['location'] + fantastic_conf['file']))
    except Exception as e:
        print(e)
        raise

    # Download marketing.ods and convert to csv
    try:
        marketing_conf.update(etl_e.download_ods_convert_to_csv(
            source = marketing_conf['source'],
            location = marketing_conf['location'],
            file = marketing_conf['file']))
    except Exception as e:
        print(e)
        raise


def control_data(conn):
    # TODO : dontrol code
    try:
        cursor = conn.cursor()
        results = []
        for check in [sql_queries.fbde_ctrl_departement, sql_queries.fbde_ctrl_ticket,
                      sql_queries.fbde_ctrl_catalogue, sql_queries.fbde_ctrl_magasin]:
            cursor.execute(check)
            results += cursor.rowcount

        cursor.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)


def create_etl(conn):
    print('get datas')
    recover_data(conn)
    print('create E zone')
    etl_e.create_e_zone(conn)

    control_data_e(conn)



    # Get datas and create T Zone ---------------------------------------------
    print('create T zone')
    etl_t.create_t_zone(conn)

    control_data_t(conn)



    # Get datas and create L Zone ---------------------------------------------
    print('create L zone')
    etl_l.create_l_zone(conn)

    control_data_l(conn)


def sql_request(conn, query):
    # Connection to database
    cursor = conn.cursor()

    # execute a query
    try:
        cursor.execute(query)
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        sys.exit(error)

    # Close cursor and connexion
    cursor.close()



if __name__ == "__main__":

    # Get datas and create E Zone ---------------------------------------------
    db_utc = db_credentials.db_utc
    conn = None
    print('database connexion')
    print(variables.password + ' ' + variables.datawarehouse_name + ' ' + variables.utc_host)
    try:
        conn = psycopg2.connect(host=variables.utc_host,database=variables.datawarehouse_name,
                                user=variables.user, password=variables.password)
    except (Exception, psycopg2.DatabaseError) as error:
        sys.exit(error)

    # create_etl(conn)
    sql_request(conn, sql_queries.my_query)

    if conn is not None:
        conn.close()


print(db_credentials.ods_db_fantastic_config)
print(db_credentials.oracle_db_catalogue_config)
print(db_credentials.ods_db_marketing_config)
print(db_credentials.csv_db_departement_config)
