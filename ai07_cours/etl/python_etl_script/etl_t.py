# -*- coding: utf-8 -*-

# File Name     : etl_t.py
# Description   : Code to create transform (T) zone of my ETL
#
# Requirements  : psycopg2, sys
#
# Version       : 1.0
# Created on    : Mon Mar 11 14:57:38 2019
#
# @author       : Thibault Santonja
# git depot     : https://gitlab.utc.fr/tsantonj/ai07
#
# =============================================================================

import psycopg2
import sys
# personal files
import sql_queries


def create_t_zone(conn):
    # Connection to database
    cursor = conn.cursor()

    # execute create schema and table
    cursor.execute(sql_queries.create_fbdt)
    conn.commit()
    try:
        cursor.execute(sql_queries.fbdt_copy_departement)
        cursor.execute(sql_queries.fbdt_copy_magasin)
        # cursor.execute(sql_queries.fbdt_copy_catalogue)
        # cursor.execute(sql_queries.fbdt_copy_ticket)
    except (Exception, psycopg2.DatabaseError) as error:
        sys.exit(error)


    # TODO

    return 0
