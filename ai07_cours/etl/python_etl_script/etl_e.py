# -*- coding: utf-8 -*-

# File Name     : etl_e.py
# Description   : Code to create extract (E) zone of my ETL
#
# Requirements  : cx_Oracle, csv, pandas, wget, pyexcel, pyexcel-ods, psycopg2
#
# Version       : 1.0
# Created on    : Sat Mar  9 12:28:52 2019
#
# @author       : Thibault Santonja
# git depot     : https://gitlab.utc.fr/tsantonj/ai07
#
# =============================================================================

import cx_Oracle
import csv
import pandas as pd
import wget
import pyexcel
import psycopg2
import sys
# personal files
import sql_queries


def create_e_zone(conn):
    # Connection to database
    cursor = conn.cursor()

    # execute create schema and table
    cursor.execute(sql_queries.create_fbde)
    conn.commit()

    # copy datas
    fbde_copies = [sql_queries.fbde_copy_departement, sql_queries.fbde_copy_ticket,
                   sql_queries.fbde_copy_catalogue, sql_queries.fbde_copy_magasin]

    for copy in fbde_copies:
        try:
            cursor.execute(copy)
        except (Exception, psycopg2.DatabaseError) as error:
            print("etl_e.py:create_e_zone:fbde_copy error")
            sys.exit(error)

    conn.commit()

    # Close cursor and connexion
    cursor.close()

def extract_from_oracle(host, port, sid, user, password, database, table, file):
    try:
        # Connection to database Oracle
        dsn_tns = cx_Oracle.makedsn(host, port, sid)
        connexion = cx_Oracle.connect(user, password, dsn_tns)

        # Query to extract datas
        query = "SELECT * FROM " + database + "." + table
        df = pd.read_sql(query, con=connexion)

        # Write every row in file
        df.to_csv(file, sep=';', encoding='utf-8',
                  header=None, index=False, quoting=csv.QUOTE_NONNUMERIC, quotechar='"')


        # Close the file and the connexion to Oracle database
        connexion.close()

        return {'column': list(df), 'file_size': df.size}
    except Exception as e:
        raise


def html_content_extract(source, file):
    # Open an url and extract data in a dataframe
    df = pd.read_csv(source, header=None, delimiter=';', encoding='utf-8')

    # Write the response in the .csv
    df.to_csv(file, sep=';', encoding='utf-8',
              header=None, index=False, quoting=csv.QUOTE_NONNUMERIC, quotechar='"')

    return {'column': list(df), 'file_size': df.size}


def download_ods_convert_to_csv(source, location, file):
    # download the source
    wget.download(source, location)

    # convert file to csv
    sheet = pyexcel.get_sheet(file_name=location + file, name_columns_by_row=0)
    sheet.save_as(filename= location + file.split('.')[0] + '.csv')

    return {'column': list(sheet.colnames)}


"""
def extract_from_oracle(db_config, table):
    # Create the file to stock datas and init the cursor to write in it
    file = open(db_config['location'] + db_config['file'], 'w+')
    writer = csv.writer(file)

    # Connection to database Oracle
    dsn_tns = cx_Oracle.makedsn(db_config['host'], db_config['port'], db_config['sid'])
    db = cx_Oracle.connect(db_config['user'], db_config['password'], dsn_tns)
    cursor = db.cursor()

    # Query to extract datas
    querry = "SELECT * FROM " + db_config['database'] + "." + table
    cursor.execute(querry)

    # Extract column name
    column_name = [row[0] for row in cursor.description]
    db_config.update({'column': column_name})

    # Write every row in file
    for row in cursor:
        writer.writerow(row)

    # Close the file and the connexion to Oracle database
    file.close()
    cursor.close()
    db.close()
 """
