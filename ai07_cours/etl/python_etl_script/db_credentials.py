# -*- coding: utf-8 -*-

# File Name     : db_credentials.py
# Description   : This file contains all sources databases informations
#
# Requirements  : 
#
# Version       : 1.0
# Created on    : Sat Mar  9 12:31:06 2019
# 
# @author       : Thibault Santonja
# git depot     : https://gitlab.utc.fr/tsantonj/ai07
#
# =============================================================================

# personal files
import variables

db_utc = {
    'host'      : variables.utc_host,
    'user'      : variables.user,
    'password'  : variables.password,
    'database'  : variables.datawarehouse_name
}

# Catalogue de livre: base Oracle
oracle_db_catalogue_config = {
    'host'      : variables.utc_host,
    'port'      : '1521',
    'sid'       : 'nf26',
    'database'  : 'bdd1p199',
    'user'      : variables.user,
    'password'  : variables.password,
    'file'      : 'catalogue.csv',
    'location'  : '../../datas/',
}


# Base de données des départements francais: data.gouv
csv_db_departement_config = {
    'file'      : 'departement_insee_2003.csv',
    'location'  : '../../datas/',
    'delimiter' : ';',
    'quote'     : '"'
}

# Liste des magasins: http://pic.crzt.fr/ai07/fantastic/ marketing.ods
ods_db_marketing_config = {
    'source' : 'http://pic.crzt.fr/ai07/fantastic/marketing.ods',
    'file'      : 'marketing.ods',
    'location'  : '../../datas/'
}

# Journal des ventes:  http://pic.crzt.fr/ai07/fantastic/ Fantastic
ods_db_fantastic_config = {
    'source' : 'http://pic.crzt.fr/ai07/fantastic/Fantastic',
    'file'      : 'fantastic.csv',
    'location'  : '../../datas/'
}
