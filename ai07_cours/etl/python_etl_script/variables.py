# -*- coding: utf-8 -*-

# File Name     : variables.py
# Description   : This file contains all global variables of this project.
#
# Requirements  :
#
# Version       : 1.0
# Created on    : Sat Mar  9 12:28:31 2019
#
# @author       : Thibault Santonja
# git depot     : https://gitlab.utc.fr/tsantonj/ai07
#
# =============================================================================

datawarehouse_name = 'dbbdd1p002'
utc_host = 'tuxa.sme.utc'

user = 'bdd1p002'
password = None
