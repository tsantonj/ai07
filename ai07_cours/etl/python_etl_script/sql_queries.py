# -*- coding: utf-8 -*-

# File Name     : sql_queries.py
# Description   : store all sql queries for extracting from source databases
#               and loading into the target database
#
# Requirements  :
#
# Version       : 1.0
# Created on    : Sat Mar  9 12:29:09 2019
#
# @author       : Thibault Santonja
# git depot     : https://gitlab.utc.fr/tsantonj/ai07
#
# =============================================================================

my_query = ('''

''')


# ----------------------------------------------------------------------------
# ---------------------------- E zone SQL scripts ----------------------------
# ----------------------------------------------------------------------------
# SQL to create the schema and tables
create_fbde = ('''
DROP SCHEMA IF EXISTS fbde CASCADE;
CREATE SCHEMA fbde
  CREATE TABLE Catalogue (
    ref text,
    isbn text,
    title text,
    authors text,
    language text,
    pubdate text,
    publisher text,
    genre text,
    tags text
  )

  CREATE TABLE departement (
    Numero text,
    Nom text,
    Population text
  )

  CREATE TABLE Magasin (
    Magasin text,
    Departement text,
    Rayonnage text,
    Rayonnage_full text,
    R_BestSeller text,
    R_Recent text
  )

  CREATE TABLE Ticket (
    Numero text,
    Dt text,
    Produit text,
    Magasin Text
  )

  GRANT ALL PRIVILEGES ON Catalogue, Departement, Magasin, Ticket TO bdd1p199;
''')

# SQL to copy CSV datas in table
fbde_copy_departement = ('''\copy fbde.departement FROM '~/Documents/ai07/datas/departement_insee_2003.csv' DELIMITER ';' CSV;''')
fbde_copy_ticket = ('''\copy fbde.ticket FROM '~/Documents/ai07/datas/fantastic.csv' DELIMITER ';' CSV;''')
fbde_copy_catalogue = ('''\copy fbde.catalogue FROM '~/Documents/ai07/datas/catalogue.csv' DELIMITER ',' CSV;''')
fbde_copy_magasin = ('''\copy fbde.magasin FROM '~/Documents/ai07/datas/marketing.csv' DELIMITER ',' CSV;''')

# SQL script to control
fbde_ctrl_departement = ('''SELECT COUNT(*) FROM fbde.departement;''')
fbde_ctrl_ticket = ('''SELECT COUNT(*) FROM fbde.ticket;''')
fbde_ctrl_catalogue = ('''SELECT COUNT(*) FROM fbde.catalogue;''')
fbde_ctrl_magasin = ('''SELECT COUNT(*) FROM fbde.magasin;''')


# ----------------------------------------------------------------------------
# ---------------------------- T zone SQL scripts ----------------------------
# ----------------------------------------------------------------------------
create_fbdt = ('''
DROP SCHEMA IF EXISTS fbdt CASCADE;
CREATE SCHEMA fbdt
    CREATE TABLE ventes (
      fk_date     date,
      fk_magasin  varchar(4),
      fk_isbn     char(13)
    )

  CREATE TABLE magasins (
    pk_magasin  text,       % TODO clean de la dernière "data" ALTER COLUMN varchar(4)
    rayon       char(1) CHECK (rayon IN ('A', 'E', 'Y')),
    r_full      char(7),
    r_bs        boolean,
    r_new       boolean,
    dep_num     char(2),    % TODO rajouter les 0 devant
    dep_nom     varchar(255),
    dep_pop     integer,
    reg_nom     varchar(255)
  )

  CREATE TABLE dates (      % TODO Extraire les données de la pk
    pk_date     varchar(10), % TODO Traiter la date
    annee       numeric(4),
    mois        numeric(2),
    jour        numeric(2),
    trimestre   numeric(1),
    semestre    numeric(1),
    j_semaine   varchar(10)
  )

  CREATE TABLE livres (
    pk_isbn     varchar(13),    % TODO transformer en char(13)
    reference   varchar(255),
    titre       varchar(255),
    auteur      varchar(255),
    editeur     varchar(255),
    genre       varchar(255),
    langage     varchar(3),
    anne_pub    numeric(4),
    age         numeric(4)
  )

  GRANT ALL PRIVILEGES ON ventes, magasins, dates, livres TO bdd1p199;
''')

fbdt_data_ventes = None # todo, recup pk
#INSERT INTO fbdt.ventes(fk_date, fk_isbn, fk_magasin)
#  SELECT CAST(dt AS varchar(10)), CAST(produit AS varchar(13)), CAST(magasin AS varchar(4))
#  FROM fbde.ticket;
#''')

fbdt_data_magasins = ('''
INSERT INTO fbdt.magasins(pk_magasin, rayon, r_full, r_bs, r_new, dep_num)
  SELECT magasin, rayonnage, rayonnage_full,
    CASE WHEN r_bestseller='0' THEN False WHEN r_bestseller='1' THEN True END,
    CASE WHEN r_recent='0' THEN False WHEN r_recent='1' THEN True END,
    departement
  FROM fbde.magasin;

UPDATE fbdt.magasins SET
  dep_nom = fbde.departement.nom,
  dep_pop = CAST(fbde.departement.population AS numeric(9))
  FROM fbde.departement
  WHERE fbdt.magasins.dep_num = CAST(CAST(fbde.departement.numero AS integer) AS CHAR(2));
''')

fbdt_data_dates = ('''
INSERT INTO fbdt.dates(pk_date)
  SELECT DISTINCT(dt)
  FROM fbde.ticket;
''')


fbdt_data_livres = ('''

''')

# ----------------------------------------------------------------------------
# ---------------------------- L zone SQL scripts ----------------------------
# ----------------------------------------------------------------------------
create_fbdl = ('''
DROP SCHEMA IF EXISTS fbdl CASCADE;
CREATE SCHEMA fbdl
  CREATE TABLE ventes (
    fk_date     date,
    fk_magasin  varchar(4),
    fk_isbn     char(13)
  )

  CREATE TABLE magasins (
    pk_magasin  varchar(4),
    rayon       char,
    r_bs        boolean,
    r_new       boolean,
    dep_num     numeric(2),
    dep_nom     varchar(255),
    Population  integer,
    reg_nom     varchar(255)
  )

  CREATE TABLE dates (
    pk_date     date,
    annee       numeric(4),
    mois        numeric(2),
    jour        numeric(2),
    trimestre   numeric(1),
    semestre    numeric(1),
    j_semaine   varchar(10)
  )

  CREATE TABLE livres (
    pk_isbn     char(13),
    reference   varchar(20),
    titre       varchar(255),
    auteur      varchar(255),
    editeur     varchar(255),
    genre       varchar(255),
    langage     char(3),
    anne_pub    numeric(4),
    age         integer
  )

  GRANT ALL PRIVILEGES ON livres, dates, magasins, ventes TO bdd1p199;
''')

insert_fbdl = ('''
  INSERT INTO fbdl.ventes(fk_date, fk_isbn, fk_magasin)
    SELECT fk_date, fk_isbn, fk_magasin
    FROM fbdt.ventes;

  -- données magasins ----------------------------------------------------------
  INSERT INTO fbdl.magasins
    SELECT pk_magasin, rayon, r_full,r_bs, r_new,dep_num
    FROM fbdt.magasins;

  -- données dates -------------------------------------------------------------------
  INSERT INTO fbdl.dates(pk_date)
    SELECT pk_date
    FROM fbdt.dates;
''')
