--DROP TABLE fbde.catalogue, fbde.departement, fbde.magasin, fbde.ticket;
--DROP SCHEMA fbde;

-- Creation du schema et des tables
-- Ajout de privilèges à bdd1p199
CREATE SCHEMA fbde
  CREATE TABLE catalogue (
    ref text,
    isbn text,
    title text,
    authors text,
    language text,
    pubdate text,
    publisher text,
    genre text,
    tags text
  )

  CREATE TABLE departement (
    Numero text,
    Nom text,
    Population text
  )

  CREATE TABLE magasin (
    Magasin text,
    Departement text,
    Rayonnage text,
    Rayonnage_full text,
    R_BestSeller text,
    R_Recent text
  )

  CREATE TABLE ticket (
    Numero text,
    Dt text,
    Produit text,
    Magasin Text
  )

  GRANT ALL PRIVILEGES ON Catalogue, Departement, Magasin, Ticket TO bdd1p199;

-- import / copy des CSV dans les tables précédemment créées
\copy fbde.departement FROM '~/Documents/ai07/datas/departement_insee_2003.csv' DELIMITER ';' CSV;
\copy fbde.ticket FROM '~/Documents/ai07/datas/fantastic.csv' DELIMITER ';' CSV;
\copy fbde.catalogue FROM '~/Documents/ai07/datas/catalogue.csv' DELIMITER ',' CSV;
\copy fbde.magasin FROM '~/Documents/ai07/datas/marketing.csv' DELIMITER ',' CSV;
