Notes d'AI07 - NF26 : **DATA WAREHOUSE**
===

**INFORMATION GÉNÉRALES**
- [Mattermost: team.picasoft.net/AI07](https://team.picasoft.net/ai07/channels/town-square)
- [Lien vers le pad: //pad.picasoft.net/p/ai07](https://pad.picasoft.net/p/ai07)
- [Link to the README.md](README.md)
- [Link to some useful notes](notes.md)

# Introduction au domaine du décisionnel et aux data warehouses

## Le décisionnel

Le but des Data Warehouse (**BW**) est de répondre à des question que l'on ne connait pas encore. Ils stockent une grande quantité d'informations, dans le but d'être croisés dans des outils décisionnels, afin d'aider la prise de décisions dans une entreprise (**Business Intelligent ou BI**).

Ces données sont:
- **éparpillés** dans divers services de l'entreprise
- **hétérogènes** (dans le format, l'organisation, la <span class="bg-info text-primary">sémantique</span>)
- **volatiles** (rapidement obselètes)
- **implémentés** (pour l'action et non pour l'analyse)


> exemple d'outils, Kibana, Elasticserch, Grafana


### Danger des outils décisionnels


:::danger
___ATTENTION, en décisionnel, l'important n'est pas le système mais l'influence sur la décision. Sachant que les données sont partielles, les indicateurs ne sont qu'une vision de la réalité. Les décisions prises suite à cette vision de la réalité sont souvent très lourde.
De même, ne connaissant ni l'input ni l'output, ATTENTION AUX DECISIONS PRISES !___
:::


Surqualifier le code et bien comprendre son fonctionnement pour éviter les erreurs invisibles et graves.

Attention à se conformer à la **RGPD**. Il faut informer de ce que je fais des données et respecter ce que je dis.

### Exploitation des données

![](https://librecours.net/module/dwh/int01/res/architecture.png "Architecture classique des outils décisionnels")

On peut voir ici qu'un **ETL** (Extraction, Transformation, Loading), extrait les données des DB de l'entreprise pour **nourrir le DW**. Ensuite, l'exploitation du DW consiste à créer des **DM** (Data Mart) regrouppant une partie des informations du DW dont on se servira ensuite. On observe 3 grandes catégories d'exploitation des DM:
- **Reporting**, création d'indicateurs, graphiques... Il est souvent récurrent pour suivre l'évolution de l'indicateur
- L'**Exploration** manuelle, consiste à trouver des questions auxquelles on peut répondre
- L'**Analyse** de données, chercher des corrélations entre les datas via des stats, faire du prédictif, ...

[Quelques exemples d'outils OpenSource](https://librecours.net/module/dwh/int01/intUC041outils.xhtml)


### Conception d'un DW

1. Étude des besoins et de l'existant
    - Étude des besoins utilisateurs
    - Étude des données existantes

2. Modélisation et conception
    - Modélisation dimensionnelle
    - Architecture technique
    - Spécification des outils d'exploitation

3. Implémentation du data warehouse
    - Implémentation du DW[1] et des DM[2]
    - Mise en place de l'ETL[3]

4. Implémentation des outils d'exploitation
    - Implémentation des outils de reporting
    - Implémentation des outils d'exploration
    - Implémentation des outils de prédiction



## Le Data Warehouse

Un DW:
- **intègration** de beaucoup de datas
- **historisation** les données dans le temps
- **optimisation** de l'organisation des données pour un accès rapide et simple


:::info
___Là où les BD classiques, **transactionnelles**, ont pour but d'être mises à jour et utilisées en **temps réel**, les **Data Warehouse** ont pour but d'être interrogée sur une question **statique**.___
:::

Le but d'un DW est d'être **simple** et **optimisé**. Comment rester performant et lisible ? On utilise massivement:
- des **vues**, copie depuis les systèmes transactionnels
- la **dénormailastion**, extrèmement redondant

> Pour rappel, un DW étant **statique**, il ne souffre pas des inconvénients des systèmes transactionnels

Des DW sont extrait des DM. Ceux ci regroupent une partie des données du DW, afin de les organiser de façon exploitable pour un groupe de questions données:
- un DM pour les RH
- un DM pour la R&D
- ....
A noter que tous ces DM tirent leurs données du **même DW**.


## Le modèle en étoile

> Représentation fortement **dénormalisée**, très **performant**

Ce modèle consiste à avoir une table centrale (= **fait**) contenant de nombreuses clef étrangères vers d'autres tables fortement redondante (= **dimensions**).

![](https://librecours.net/module/dwh/int01/res/modeleEnEtoile_1.png "Exemple d'architecture en étoile.")

Le modèle est donc de dimension 1. Si l'on rajoute des tables aux dimensions, nous retrouvons un modèle en **flocon**.

Ce type de modèle permet une interrogation rapide des données en **une seule opération de jointure**

Le but du modèle en étoile (et de tous bon modèle décisionnel) est de :
- Être **performant** pour les agrégats de gros volumes de data (exploration de données, ___reporting___)
- Être **compréhensible** par l'utilisateur final, afin d'écrire facilement et rapidement des requêtes
- Être assez performant aux mises à jour
- Être évolutif en fonction des évolutions des entrées et des besoins en sortie


# Les ETL
Converti les datas réelles vers le DW. Les données seront nettoyées:
- Rajoutées s'il en manque dans les data de base et que l'on peut els trouver ailleurs (il faut que cela soit pertinent)
- Mises à jours
- Supprimées si inutilisable et non pertinentes


> Les ETL sont généralement longs à faire, il faut donc impérativement traiter l'important rapide à faire, puis se pencher sur les points plus longs

On peut développer un ETL de A à Z = **ex-nihilo**, ou utiliser des outils existant (Talend Open Studio, Dentaho Data Integration, ...)
Le premier offre:
- de la flexibilité
- de la performance
- de l'adaptabilité

Le second:
- des beaux graphismes
- de la facilité
- de la gestion d'erreur
- des API dédiées pour tout gérer (connexions, imports,...)

On peut relever 3 zones dans un ETL:
- **E** pour extraction, qui consiste à simplement récupérer les données
- **T** pour transformation, où l'on va nettoyer les données
- **L** pour exploitation, où l'on créera le DW et ses DM (modèles en étoiles)


---

# Cas Fantastic

> Étude de cas dans le dossier `mod`

## Requête décisionnelle

Une **Requête décisionnelle** exprime toujours une quantification de **fait** par rapport à des **dimensions** `Quelle a été la quantité de ... (fait) en fonction de ... (dimension)`

**Un _fait_ est une grandeur que l'on souhaite mesurer (prix, quantité, ...)**
**Une _dimension_ est un axe d'analyse (date, lieu, personne, ...)**

<<<<<<< HEAD


## rapportSQL
```
dbbdd1p002=> CREATE TABLE fbde.t_catalogue AS
dbbdd1p002-> TABLE fbde.catalogue;
SELECT 1443
dbbdd1p002=> CREATE TABLE fbde.t_departement AS
dbbdd1p002-> TABLE fbde.departement;
SELECT 95
dbbdd1p002=> CREATE TABLE fbde.t_magasin AS
dbbdd1p002-> TABLE fbde.magasin;
SELECT 152
dbbdd1p002=> CREATE TABLE fbde.t_ticket AS
dbbdd1p002-> TABLE fbde.ticket;
SELECT 512018
```
=======
## correction partie 1
Ajouter les dépendances fonctionnelles pour contrôler le modèle, voir les redondances inutiles...

Table `Département` inutile et fausse car info en double inutile

Table `Date` pertinance des secondes ? surtout que l'on n'a pas la donnée

Table `Magasin` pleins de datas sur le département peuvent être récupérée qui peuvent être plus intéressante notamment le nom de la **région** !

Table `Livre` rajouter l'age plutot que la date de publication, plus pertinent et rapide à analyser.

> Toujours se demander quelles informations sont récupérables et si elles sont pertinente !
>>>>>>> 461ed2c6741c371abc69a38f3e8076f8efffc715
